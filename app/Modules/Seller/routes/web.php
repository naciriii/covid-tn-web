<?php

Route::group(['module' => 'Seller', 'middleware' => ['web'], 'namespace' => 'App\Modules\Seller\Controllers'], function() {

    Route::resource('Seller', 'SellerController');

});

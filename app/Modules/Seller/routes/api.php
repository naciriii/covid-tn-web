<?php

Route::group(['module' => 'Seller', 'middleware' => ['api'], 'namespace' => 'App\Modules\Seller\Controllers'], function() {

    Route::resource('Seller', 'SellerController');

});

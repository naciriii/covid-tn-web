@extends('frontOffice.layout')

@section('head')
    @include('frontOffice.inc.head',
    ['title' => config('app.name') . ' - Accueil',
    'description' => 'Accueil - ' . config('app.name')
    ])
@endsection

@section('header')
    @include('frontOffice.inc.header')
@endsection

@section('sidebar-left')
    @include('frontOffice.inc.sidebar.left', [
        'current' => 'home'
    ])
@endsection

@section('content')
<div class="wrap-fluid">
    <div class="container-fluid paper-wrap bevel tlbr">

        <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatable/datatable.css') }}">
        <script type="text/javascript" src="{{ asset('plugins/datatable/datatable.js') }}"></script>

        <!-- CONTENT -->
        <!--TITLE -->
        <div class="row">
            <div id="paper-top">
                <div class="col-lg-3">
                    <h2 class="tittle-content-header">
                        <i class="icon-window"></i>
                        <span>Annonces
                            </span>
                    </h2>

                </div>

                <div class="col-lg-9">
                    <div class="devider-vertical visible-lg"></div>
                    <div class="tittle-middle-header">

                        <div class="alert">
                            <span class="tittle-alert entypo-info-circled"></span>
                            Pour plus d'informations ou pour déclarer un cas d'infection, appelez le numéro vert  <strong>80 10 19 19</strong>
                        </div>
                    </div>

                </div>
            </div>
        </div>        <!--/ TITLE -->

        <!-- BREADCRUMB -->
        <ul id="breadcrumb">
            <li>
                <span class="entypo-home"></span>
            </li>
            <li><i class="fa fa-lg fa-angle-right"></i>
            </li>
            <li><a href="{{ route('showHome') }}" title="Sample page 1">Accueil</a>
            </li>
        </ul>

        <!-- END OF BREADCRUMB -->



        <script>
            // Initialize and add the map
            function initMap() {
                /* Datatables responsive */

                $(document).ready(function () {
                    $('#datatable-responsive').DataTable({
                        responsive: true,
                        language: {
                            url: "{{ asset('plugins/datatable/lang/fr.json') }}"
                        }
                    });
                    $('.dataTables_filter input').attr("placeholder", "Rechercher...");
                });

                tunisia = {lat: 34.2651487, lng: 9.4225926};

                map = new google.maps.Map(
                    document.getElementById('map'),
                    {zoom: 8, center: tunisia, styles: [
                            {
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#212121"
                                    }
                                ]
                            },
                            {
                                "elementType": "labels.icon",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#757575"
                                    }
                                ]
                            },
                            {
                                "elementType": "labels.text.stroke",
                                "stylers": [
                                    {
                                        "color": "#212121"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#757575"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative.country",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#9e9e9e"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative.land_parcel",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative.locality",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#bdbdbd"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#757575"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi.medical",
                                "elementType": "geometry.fill",
                                "stylers": [
                                    {
                                        "color": "#ff122f"
                                    },
                                    {
                                        "lightness": 100
                                    },
                                    {
                                        "weight": 8
                                    }
                                ]
                            },
                            {
                                "featureType": "poi.medical",
                                "elementType": "geometry.stroke",
                                "stylers": [
                                    {
                                        "color": "#ff122f"
                                    },
                                    {
                                        "lightness": 100
                                    },
                                    {
                                        "weight": 8
                                    }
                                ]
                            },
                            {
                                "featureType": "poi.medical",
                                "elementType": "labels.icon",
                                "stylers": [
                                    {
                                        "color": "#ff122f"
                                    },
                                    {
                                        "lightness": 100
                                    }
                                ]
                            },
                            {
                                "featureType": "poi.medical",
                                "elementType": "labels.text",
                                "stylers": [
                                    {
                                        "color": "#ff122f"
                                    },
                                    {
                                        "lightness": 100
                                    }
                                ]
                            },
                            {
                                "featureType": "poi.park",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#181818"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi.park",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#616161"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi.park",
                                "elementType": "labels.text.stroke",
                                "stylers": [
                                    {
                                        "color": "#1b1b1b"
                                    }
                                ]
                            },
                            {
                                "featureType": "road",
                                "elementType": "geometry.fill",
                                "stylers": [
                                    {
                                        "color": "#2c2c2c"
                                    }
                                ]
                            },
                            {
                                "featureType": "road",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#8a8a8a"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.arterial",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#373737"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#3c3c3c"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.highway.controlled_access",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#4e4e4e"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.local",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#616161"
                                    }
                                ]
                            },
                            {
                                "featureType": "transit",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#757575"
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#000000"
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#3d3d3d"
                                    }
                                ]
                            }
                        ]});
                // The marker, positioned at Uluru
                @foreach($volunteers as $volunteer)
                    volunteer{{ $loop->iteration }} = new google.maps.Marker(
                    {
                        position: {
                            lat: {{ $volunteer->lat }}, lng: {{ $volunteer->lng }}
                        },
                        map: map,
                        icon: {
                            url: "{{ asset('img/volun.png') }}"
                        }
                    });

                volunteerInfoWindow{{ $loop->iteration}} = new google.maps.InfoWindow({
                    content: '<div id="content">'+
                        '<div id="siteNotice">'+
                        '</div>'+
                        '<h4 style="text-decoration: underline;"> {{ $volunteer->user->name }} </h3>'+
                        '<div id="bodyContent">'+

                                    @foreach($volunteer->categories as $category)
                                '<a class="tooltip-tip ajax-load tooltipster-disable" style="margin-bottom:3px;">'+
                        '<i class="fa fa-plus" style="margin-right:3px;"></i>'+
                        '<span style="display: inline-block; float: none;">{{ $category->title }}</span>'+
                        '</a><br>'+
                            @endforeach
                                '<br><a href="{{ route('showVolunteerPublicProfile', ['id' => $volunteer->id]) }}" class="pull-right" style="text-decoration:underline;">Plus d\'Infos</a>'+
                                '</div>'+
                        '</div>'
                });

                google.maps.event.addListener(volunteer{{ $loop->iteration }}, 'click', function () {
                    @foreach($volunteers as $volunteer)
                    volunteerInfoWindow{{ $loop->iteration }}.close();
                    @endforeach
                    volunteerInfoWindow{{ $loop->iteration }}.open(map, volunteer{{ $loop->iteration }});
                });
                @endforeach
                @foreach($hospitals as $hospital)
                    hospital{{ $loop->iteration }} = new google.maps.Marker(
                    {
                        position: {
                            lat: {{ $hospital->lat }}, lng: {{ $hospital->lng }}
                        },
                        map: map,
                        icon: {
                            @if(count($hospital->requests) > 0)
                            url: "{{ asset('img/firstaid-2.png') }}"
                            @else
                            url: "{{ asset('img/firstaid-0.png') }}"
                            @endif
                        }
                    });

                    hospitalInfoWindow{{ $loop->iteration }} = new google.maps.InfoWindow({
                        content: '<div id="content">'+
                            '<div id="siteNotice">'+
                            '</div>'+
                            '<h4 style="text-decoration: underline;"> {{ $hospital->title }} </h3>'+
                            '<div id="bodyContent">'+
                            @if(count($hospital->requests) > 0)
                            @foreach($hospital->requests as $request)
                            '<a class="tooltip-tip ajax-load tooltipster-disable" style="margin-bottom:3px;">'+
                            '<i class="fa fa-plus" style="margin-right:3px;"></i>'+
                            '<span style="display: inline-block; float: none;">{{ $request->type->title }}</span>'+
                            '<div class="noft-blue" style="display: inline-block; float: none;margin-top: 2px;">@if(count($request->entries) > 0) {{ $request->entries->sum('quantity') }} @else 0 @endif /{{ $request->quantity }}</div>'+
                            '</a><br>'+
                            @endforeach
                                    '<br><a href="{{ route('showSingleHospital', ['id' => $hospital->id]) }}" class="pull-right" style="text-decoration:underline;">J\'AIDE</a>'+
                                @else
                                '<a class="tooltip-tip ajax-load tooltipster-disable">'+
                            '<span style="display: inline-block; float: none;">Cet Hôpital ne comporte pas de besoins annoncés.</span>'+
                            @endif
                            '</div>'+
                            '</div>'
                    });

                    google.maps.event.addListener(hospital{{ $loop->iteration }}, 'click', function () {
                        @foreach($hospitals as $hospital)
                        hospitalInfoWindow{{ $loop->iteration }}.close();
                        @endforeach
                        hospitalInfoWindow{{ $loop->iteration }}.open(map, hospital{{ $loop->iteration }});
                    });
                @endforeach

                // Try HTML5 geolocation.
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        var pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };

                        map.setCenter(pos);
                    }, function() {
                        handleLocationError(true, '', map.getCenter());
                    });
                } else {
                    // Browser doesn't support Geolocation
                    handleLocationError(false, '', map.getCenter());
                }
            }

            function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                infoWindow.setPosition(pos);
                infoWindow.setContent(browserHasGeolocation ?
                    'Error: The Geolocation service failed.' :
                    'Error: Your browser doesn\'t support geolocation.');
                infoWindow.open(map);
            }
        </script>
        <!--Load the API from the specified URL
        * The async attribute allows the browser to render the page while the API loads
        * The key parameter will contain your own API key (which is not needed for this tutorial)
        * The callback parameter executes the initMap() function
        -->
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBRJQqA2pOFX-shd3sSVlMQb9vXdAZQsjo&callback=initMap">
        </script>


        <div id="paper-middle">
            <div id="map" style="height: 100%;"></div>
        </div>

        <!--  DEVICE MANAGER -->
        <div class="content-wrap">
            <div class="row">
                <div class="col-lg-3">
                    <div class="profit" id="profitClose">
                        <div class="headline ">
                            <h3>
                                    <span>
                                        <i class="maki-warehouse"></i>&#160;&#160;Quantité Totale</span>
                            </h3>
                            <div class="titleClose">
                                <a href="#profitClose" class="gone">
                                    <span class="entypo-cancel"></span>
                                </a>
                            </div>
                        </div>

                        <div class="value">
                                <span class="pull-left"><i class="fa fa-bookmark"></i>
                                </span>
                            <div>
                                <span>{{ $requests }}</span>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class=" member" id="memberClose">
                        <div class="headline ">
                            <h3>
                                    <span>
                                        <i class="fa fa-truck"></i>
                                        &#160;&#160;REÇUS
                                    </span>
                            </h3>
                            <div class="titleClose">
                                <a href="#memberClose" class="gone">
                                    <span class="entypo-cancel"></span>
                                </a>
                            </div>
                        </div>
                        <div class="value">
                                <span class="pull-left"><i class="fa fa-hospital-o"></i>
                                </span>
                            <div>
                                <span>{{ $entries }}</span>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="order" id="orderClose">
                        <div class="headline ">
                            <h3>
                                    <span>
                                        <i class="maki-hospital"></i>&#160;&#160;HÔPITAUX</span>
                            </h3>
                            <div class="titleClose">
                                <a href="#orderClose" class="gone">
                                    <span class="entypo-cancel"></span>
                                </a>
                            </div>
                        </div>
                        <div class="value">
                                <span class="pull-left"><i class="fa fa-heart-o"></i>
                                </span>
                            <div>
                                <span>{{ count($hospitals) }}</span>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="revenue" id="revenueClose">
                        <div class="headline ">

                            <h3>
                                    <span>
                                        <i class="maki-commerical-building"></i>&#160;&#160;Volontaires</span>
                            </h3>

                            <div class="titleClose">
                                <a href="#revenueClose" class="gone">
                                    <span class="entypo-cancel"></span>
                                </a>
                            </div>
                        </div>
                        <div class="value">
                                <span class="pull-left"><i class="fa fa-gift"></i>
                                </span>
                            <div>
                                <span>{{ $volunteersCount }}</span>
                            </div>


                        </div>

                    </div>
                </div>

            </div>
        </div>
        <!--  / DEVICE MANAGER -->










        <div class="content-wrap">
            <div class="row">
                <div class="col-lg-8">
                    <div class="nest" id="Footable5Close">
                        <div class="title-alt">
                            <h6>
                                Liste des Demandes</h6>
                            <div class="titleClose">
                                <a class="gone" href="#Footable5Close">
                                    <span class="entypo-cancel"></span>
                                </a>
                            </div>
                            <div class="titleToggle">
                                <a class="nav-toggle-alt" href="#Footable5">
                                    <span class="entypo-up-open"></span>
                                </a>
                            </div>

                        </div>

                        <div class="body-nest" id="Footable5">

                            <table id="datatable-responsive"
                                   class="display table table-striped table-bordered" cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th>Titre</th>
                                    <th>Gouvernorat</th>
                                    <th>Besoins</th>
                                </tr>
                                </thead>

                                <tfoot>
                                <tr>
                                    <th>Titre</th>
                                    <th>Gouvernorat</th>
                                    <th>Besoins</th>
                                </tr>
                                </tfoot>

                                <tbody>
                                @foreach($hospitals->where('user_id', '<>', null) as $hospital)
                                    <tr>
                                        <td><a href="{{ route('showSingleHospital', ['id' => $hospital->id]) }}">{{ $hospital->title }}</a></td>
                                        <td>{{ $hospital->city->title }}</td>
                                        <td>
                                            @if(count($hospital->requests) > 0)
                                                @foreach($hospital->requests as $request)
                                                    {{ $request->type->title }} :
                                                    <strong>
                                                    @if(count($request->entries) > 0)
                                                        {{ $request->entries->sum('quantity') }}
                                                        @else 0
                                                        @endif
                                                        / {{ $request->quantity }}
                                                    </strong>
                                                    <br>
                                                @endforeach
                                            @else
                                                Cet Hôpital ne comporte pas de besoins annoncés
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
                <div class="col-lg-4">
                    <div id="RealTimeClose" class="nest">
                        <div class="title-alt">
                            <h6>
                                <span class="fontawesome-bar-chart"></span>&nbsp;Chiffres du Jour</h6>
                            <div class="titleClose">
                                <a class="gone" href="#RealTimeClose">
                                    <span class="entypo-cancel"></span>
                                </a>
                            </div>
                            <div class="titleToggle">
                                <a class="nav-toggle-alt" href="#RealTime">
                                    <span class="entypo-up-open"></span>
                                </a>
                            </div>
                        </div>
                        <div id="RealTime" style="min-height:296px;padding-top:20px;" class="body-nest">
                            <ul class="direction">
                                <li>
                                    <span class="direction-icon maki-fire-station" style="background:#FF6B6B"></span>
                                    <h3>
                                        <span>Contaminés</span>
                                    </h3>
                                    <p>{{ $statistics->count() ? $statistics->first()->cases : 0 }} <i class="fa fa-plus-circle"></i><small> @if($statistics->count() >= 2) {{ $statistics->first()->cases  - $statistics->get(1)->cases }} @else 0 @endif</small>
                                    </p>
                                    <p><i>Mise à Jour </i>:&nbsp;<i class="fa fa-clock-o"></i>&nbsp;&nbsp;{{ $statistics->count() ? $statistics->first()->created_at->format('d-m-Y') : '-' }}</p>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <span class="direction-icon maki-garden" style="background:#65C3DF"></span>
                                    <h3>
                                        <span>Rétablis</span>
                                    </h3>
                                    <p>{{ $statistics->count() ? $statistics->first()->recovered : 0 }} <i class="fa fa-plus-circle"></i><small> @if($statistics->count() >= 2) {{ $statistics->first()->recovered - $statistics->get(1)->recovered }} @else 0 @endif</small>
                                    </p>
                                    <p><i>Mise à Jour </i>:&nbsp;<i class="fa fa-clock-o"></i>&nbsp;&nbsp;{{ $statistics->count() ? $statistics->first()->created_at->format('d-m-Y') : '-' }}</p>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <span class="direction-icon maki-roadblock" style="background:#000000"></span>
                                    <h3>
                                        <span>Décès</span>
                                    </h3>
                                    <p>{{ $statistics->count() ? $statistics->first()->deaths : 0 }} <i class="fa fa-plus-circle"></i><small> @if($statistics->count() >= 2) {{ $statistics->first()->deaths - $statistics->get(1)->deaths  }} @else 0 @endif</small>
                                    </p>
                                    <p><i>Mise à Jour </i>:&nbsp;<i class="fa fa-clock-o"></i>&nbsp;&nbsp;{{ $statistics->count() ? $statistics->first()->created_at->format('d-m-Y') : '-' }}</p>
                                </li>
                                <li class="divider"></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>





        <div class="content-wrap">

            <!-- FOOTER -->
            <div id="footer">
                <div class="devider-footer-left"></div>
                <div class="time">
                    <p id="spanDate"></p>
                    <p id="clock"></p>
                </div>
                <div class="copyright">Dévéloppé avec
                    <span class="entypo-heart"></span> par <a href="https://www.bluepenlabs.com" target="_blank">BluePen Labs</a></div>
                <div class="devider-footer"></div>


            </div>
            <!-- / END OF FOOTER -->


        </div>
    </div>
</div>

@endsection


@extends('frontOffice.layout')

@section('head')
    @include('frontOffice.inc.head',
    ['title' => config('app.name') . ' - Contact',
    'description' => 'Contact - ' . config('app.name')
    ])
@endsection

@section('header')
    @include('frontOffice.inc.header')
@endsection

@section('sidebar-left')
    @include('frontOffice.inc.sidebar.left', [
        'current' => 'contact'
    ])
@endsection

@section('content')
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i>
                            <span>Annonces
                            </span>
                        </h2>

                    </div>

                    <div class="col-lg-9">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                            <div class="alert">
                                <span class="tittle-alert entypo-info-circled"></span>
                                {!! $announcement !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="{{ route('showHome') }}">Accueil</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="{{ route('showContact') }}">Contact</a>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->


            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
                <!-- PROFILE -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-lg-12 well profile">
                            <div class="col-sm-12">
                                <div class="col-xs-12 col-sm-12 profile-name">
                                    <h2>Nous Contacter
                                    </h2>
                                    <hr>
                                    <form action="{{ route('handleContact') }}" method="post">
                                        {{ csrf_field() }}
                                    <dl class="dl-horizontal-profile">
                                        <dt>Nom</dt>
                                        <dd><input class="form-control" required name="name" type="text"></dd>

                                        <dt>Email</dt>
                                        <dd><input class="form-control" required name="email" type="email"></dd>

                                        <dt>Sujet</dt>
                                        <dd><input class="form-control" required name="subject" type="text"></dd>

                                        <dt>Message</dt>
                                        <dd><textarea name="message" required class="form-control" style="height: 150px !important;"></textarea></dd>

                                        <div class="pull-right">
                                            <button type="submit" class="btn btn-primary">Envoyer</button>
                                        </div>
                                    </dl>
                                    </form>


                                    <hr>

                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <!-- END OF PROFILE -->


                <!-- /END OF CONTENT -->



                <!-- FOOTER -->
                <div class="footer-space"></div>
                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate"></p>
                        <p id="clock"></p>
                    </div>
                    <div class="copyright">Dévéloppé avec
                        <span class="entypo-heart"></span> par <a href="https://www.bluepenlabs.com" target="_blank">BluePen Labs</a></div>
                    <div class="devider-footer"></div>

                </div>
                <!-- / END OF FOOTER -->


            </div>

        </div>
    </div>

@endsection


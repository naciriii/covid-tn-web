@extends('frontOffice.layout')

@section('head')
    @include('frontOffice.inc.head',
    ['title' => config('app.name') . ' - Paramètres Généraux',
    'description' => 'Paramètres Généraux - ' . config('app.name')
    ])
@endsection

@section('header')
    @include('frontOffice.inc.header')
@endsection

@section('sidebar-left')
    @include('frontOffice.inc.sidebar.left', [
        'current' => 'general'
    ])
@endsection

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatable/datatable.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/datatable/datatable.js') }}"></script>

    <script type="text/javascript">

        /* Datatables responsive */

        $(document).ready(function () {
            $('#datatable-responsive').DataTable({
                responsive: true,
                language: {
                    url: "{{ asset('plugins/datatable/lang/fr.json') }}"
                }
            });
            $('.dataTables_filter input').attr("placeholder", "Rechercher...");
        });

    </script>

    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i>
                            <span>Annonces
                            </span>
                        </h2>

                    </div>

                    <div class="col-lg-9">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                            <div class="alert">
                                <span class="tittle-alert entypo-info-circled"></span>
                                {!! $announcement !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="{{ route('showHome') }}">Accueil</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="{{ route('showManagerGeneralConfiguration') }}">Général</a>
                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->


            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
                <div class="row">


                    <div class="col-sm-12">

                        <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <h6>
                                    Liste des Mise à Jour</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#FootableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#Footable">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="Footable">

                                <table id="datatable-responsive"
                                       class="display table table-striped table-bordered" cellspacing="0"
                                       width="100%">
                                    <thead>
                                    <tr>
                                        <th>Cas</th>
                                        <th>Décés</th>
                                        <th>Rétablis</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tfoot>
                                    <tr>
                                        <th>Cas</th>
                                        <th>Décés</th>
                                        <th>Rétablis</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>

                                    <tbody>
                                    @foreach($statistics as $info)
                                        <tr>
                                            <td>{{ $info->cases }}</td>
                                            <td>{{ $info->deaths }}</td>
                                            <td>{{ $info->recovered }}</td>
                                            <td>{{ $info->created_at->format('d-m-Y') }}</td>
                                            <td style="text-align: center">
                                                <span class="badge badge-pill " title="Modifier" data-toggle="modal" data-target="#edit-{{ $info->id }}"><i class="fa fa-edit"></i></span>
                                                <a href="{{ route('handleManagerDeleteStatistics', ['id' => $info->id]) }}"><span class="badge badge-pill warning-color"><i class="fa fa-trash-o"></i></span></a>
                                            </td>
                                        </tr>
                                        <div class="modal fade" id="edit-{{ $info->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <form method="post" action="{{ route('handleManagerEditStatistics', ['id' => $info->id]) }}">
                                                        {{ csrf_field() }}
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Modifier {{ $info->created_at->format('d-m-Y') }}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-lg-4">
                                                                    <label class="control-label">Cas</label>
                                                                    <input name="cases" required class="form-control" type="number" value="{{ $info->cases }}">
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <label class="control-label">Décés</label>
                                                                    <input name="deaths" required class="form-control" type="number" value="{{ $info->deaths }}">
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <label class="control-label">Rétablis</label>
                                                                    <input name="recovered" required class="form-control" type="number" value="{{ $info->recovered }}">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                                                            <button type="submit" class="btn btn-primary">Envoyer</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>


                    <div class="col-sm-6">

                        <div class="nest" id="Footable2Close">
                            <div class="title-alt">
                                <h6>
                                    Mise à Jour</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#Footable2Close">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#Footable2">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="Footable2">

                                <form class="form-horizontal" role="form" action="{{ route('handleManagerAddStatistics') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Cas:</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" required value="1" name="cases" type="number" min="0">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Décès:</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" required value="1" name="deaths" type="number" min="0">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Rétablis:</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" required value="1" name="recovered" type="number" min="0">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div class="col-md-8">
                                            <input class="btn btn-primary" value="Ajouter" type="submit">
                                            <span></span>
                                            <input class="btn btn-default" value="Annuler" type="reset">
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>


                    </div>
                    <div class="col-sm-6">

                        <div class="nest" id="Footable3Close">
                            <div class="title-alt">
                                <h6>
                                    Annonce</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#Footable3Close">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#Footable3">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="Footable3">

                                <form class="form-horizontal" role="form" action="{{ route('handleManagerEditAnnouncement') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Annonce:</label>
                                        <div class="col-lg-8">
                                            <textarea class="form-control" style="height: 132px !important;" required  name="message"> {{ $announcement }}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div class="col-md-8">
                                            <input class="btn btn-primary" value="Ajouter" type="submit">
                                            <span></span>
                                            <input class="btn btn-default" value="Annuler" type="reset">
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>


                    </div>

                </div>
            </div>            <!--  / DEVICE MANAGER -->


            <div class="content-wrap">
                <!-- FOOTER -->
                <div class="footer-space"></div>
                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate"></p>
                        <p id="clock"></p>
                    </div>
                    <div class="copyright">Dévéloppé avec
                        <span class="entypo-heart"></span> par <a href="https://www.bluepenlabs.com" target="_blank">BluePen Labs</a>
                    </div>
                    <div class="devider-footer"></div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
    </div>

@endsection

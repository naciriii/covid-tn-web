<?php

namespace App\Modules\General\Controllers;

use App\Modules\General\Models\General;
use App\Modules\General\Models\GeneralStatistics;
use App\Modules\Hospital\Models\Hospital;
use App\Modules\Hospital\Models\HospitalRequest;
use App\Modules\Hospital\Models\HospitalRequestEntry;
use App\Modules\Volunteer\Models\Volunteer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Kamaln7\Toastr\Facades\Toastr;

class WebController extends Controller
{
    public function showHome(){

        return view('General::frontOffice.public.home', [
            'hospitals' => Hospital::all(),
            'requests' => HospitalRequest::where('status', 1)->sum('quantity'),
            'entries' => HospitalRequestEntry::all()->sum('quantity'),
            'announcement' => General::all()[0]->announcement,
            'volunteers' => Volunteer::where('status', '=', 1)->get(),
            'volunteersCount' => Volunteer::count(),
            'statistics' => GeneralStatistics::orderBy('created_at', 'desc')->take(2)->get(),
        ]);
    }

    public function showManagerGeneralConfiguration(){
        return view('General::backOffice.list', [
            'hospitals' => Hospital::all(),
            'statistics' => GeneralStatistics::all(),
            'announcement' => General::all()[0]->announcement
        ]);
    }

    public function handleManagerEditStatistics($id, Request $request){
        $data = $request->all();

        $info = GeneralStatistics::find($id);

        $info->cases = $data['cases'];
        $info->deaths = $data['deaths'];
        $info->recovered = $data['recovered'];

        $info->save();

        Toastr::success('Statistiques modifiés');
        return back();
    }

    public function handleManagerDeleteStatistics($id){
        GeneralStatistics::delete($id);

        Toastr::warning('Statistiques supprimés');
        return back();
    }

    public function handleManagerAddStatistics(Request $request){
        $data = $request->all();

        GeneralStatistics::create([
            'cases' => $data['cases'],
            'deaths' => $data['deaths'],
            'recovered' => $data['recovered'],
        ]);

        Toastr::success('Statistiques à jour');
        return back();
    }

    public function handleManagerEditAnnouncement(Request $request){
        $data = $request->all();

        $general = General::find(1);
        $general->announcement = $data['message'];
        $general->save();

        Toastr::success('Annonces à jour');
        return back();
    }

    public function showContact(){
        return view('General::frontOffice.public.contact', [
            'announcement' => General::all()[0]->announcement
        ]);
    }

    public function handleContact(Request $request){
        $data = $request->all();

        $content = [
            'name' => $data['name'],
            'email' =>  $data['email'],
            'content' => $data['message'],
            'subject' => $data['subject']
        ];

        Mail::send('General::mail.admin', $content, function ($message) use ($data) {
            $message->from($data['email']);
            $message->to('ceo@bluepenlabs.com');
            $message->subject('Nouveau Message : ' . $data['subject']);
        });

        if(count(Mail::failures()) > 0 ) {
            Toastr::warning('L\'envoi des mails a échoué, contactez le support');
        }
        Toastr::success('Message envoyé à l\'administration');
        return redirect(route('showHome'));
    }
}

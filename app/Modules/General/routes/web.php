<?php

Route::group(['module' => 'General', 'middleware' => ['web'], 'namespace' => 'App\Modules\General\Controllers'], function() {

    Route::get('/', 'WebController@showHome')->name('showHome');
    Route::get('/contact', 'WebController@showContact')->name('showContact');
    Route::post('/contact', 'WebController@handleContact')->name('handleContact');
    Route::get('/manager/general', 'WebController@showManagerGeneralConfiguration')->name('showManagerGeneralConfiguration')->middleware('checkManagerAccess');
    Route::post('/manager/general/announcement/update', 'WebController@handleManagerEditAnnouncement')->name('handleManagerEditAnnouncement')->middleware('checkManagerAccess');
    Route::post('/manager/general/stats/add', 'WebController@handleManagerAddStatistics')->name('handleManagerAddStatistics')->middleware('checkManagerAccess');
    Route::post('/manager/general/stats/edit/{id}', 'WebController@handleManagerEditStatistics')->name('handleManagerEditStatistics')->middleware('checkManagerAccess');
    Route::get('/manager/general/stats/delete/{id}', 'WebController@handleManagerDeleteStatistics')->name('handleManagerDeleteStatistics')->middleware('checkManagerAccess');

});

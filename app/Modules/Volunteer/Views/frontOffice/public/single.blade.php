@extends('frontOffice.layout')

@section('head')
    @include('frontOffice.inc.head',
    ['title' => config('app.name') . ' - J\'AIDE',
    'description' => 'J\'AIDE - ' . config('app.name')
    ])
@endsection

@section('header')
    @include('frontOffice.inc.header')
@endsection

@section('sidebar-left')
    @include('frontOffice.inc.sidebar.left', [
        'current' => 'profile'
    ])
@endsection

@section('content')
    <link href="{{ asset('plugins/bootstrap/switch.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/iCheck/flat/all.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('plugins/iCheck/jquery.icheck.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/bootstrap/switch.js') }}"></script>

    <script>
        $(document).ready(function () {
            //CHECKBOX PRETTYFY
            $('.skin-flat input').iCheck({
                checkboxClass: 'icheckbox_flat-red',
                radioClass: 'iradio_flat-red'
            });
            $('.skin-line input').each(function () {
                var self = $(this),
                    label = self.next(),
                    label_text = label.text();

                label.remove();
                self.iCheck({
                    checkboxClass: 'icheckbox_line-blue',
                    radioClass: 'iradio_line-blue',
                    insert: '<div class="icheck_line-icon"></div>' + label_text
                });
            });
            //Switch Button
        });

        let map;
        let marker;

        function placeMarker(location) {
            if ( marker ) {
                marker.setPosition(location);
            } else {
                marker = new google.maps.Marker({
                    position: location,
                    draggable: true,
                    map: map
                });
            }
            document.getElementsByName('lat')[0].value = location.lat;
            document.getElementsByName('lng')[0].value = location.lng;
        }

        function initialize() {

            var center =  {lat: {{ $volunteer->lat }}
                , lng: {{ $volunteer->lng }} };

            map = new google.maps.Map(
                document.getElementById('map'),
                {zoom: 12, center: center, styles: [
                        {
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#212121"
                                }
                            ]
                        },
                        {
                            "elementType": "labels.icon",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#757575"
                                }
                            ]
                        },
                        {
                            "elementType": "labels.text.stroke",
                            "stylers": [
                                {
                                    "color": "#212121"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#757575"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative.country",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#9e9e9e"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative.land_parcel",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative.locality",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#bdbdbd"
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#757575"
                                }
                            ]
                        },
                        {
                            "featureType": "poi.medical",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#ff122f"
                                },
                                {
                                    "lightness": 100
                                },
                                {
                                    "weight": 8
                                }
                            ]
                        },
                        {
                            "featureType": "poi.medical",
                            "elementType": "geometry.stroke",
                            "stylers": [
                                {
                                    "color": "#ff122f"
                                },
                                {
                                    "lightness": 100
                                },
                                {
                                    "weight": 8
                                }
                            ]
                        },
                        {
                            "featureType": "poi.medical",
                            "elementType": "labels.icon",
                            "stylers": [
                                {
                                    "color": "#ff122f"
                                },
                                {
                                    "lightness": 100
                                }
                            ]
                        },
                        {
                            "featureType": "poi.medical",
                            "elementType": "labels.text",
                            "stylers": [
                                {
                                    "color": "#ff122f"
                                },
                                {
                                    "lightness": 100
                                }
                            ]
                        },
                        {
                            "featureType": "poi.park",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#181818"
                                }
                            ]
                        },
                        {
                            "featureType": "poi.park",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#616161"
                                }
                            ]
                        },
                        {
                            "featureType": "poi.park",
                            "elementType": "labels.text.stroke",
                            "stylers": [
                                {
                                    "color": "#1b1b1b"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#2c2c2c"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#8a8a8a"
                                }
                            ]
                        },
                        {
                            "featureType": "road.arterial",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#373737"
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#3c3c3c"
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway.controlled_access",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#4e4e4e"
                                }
                            ]
                        },
                        {
                            "featureType": "road.local",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#616161"
                                }
                            ]
                        },
                        {
                            "featureType": "transit",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#757575"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#000000"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#3d3d3d"
                                }
                            ]
                        }
                    ]});

            volunteerPlace = new google.maps.Marker(
                {
                    position: {
                        lat: {{ $volunteer->lat }}, lng: {{ $volunteer->lng }}
                    },
                    map: map,
                    icon: {
                        url: "{{ asset('img/volun.png') }}"
                    }
                });
        }

    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBRJQqA2pOFX-shd3sSVlMQb9vXdAZQsjo&callback=initialize">
    </script>

    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i>
                            <span>Annonces
                            </span>
                        </h2>

                    </div>

                    <div class="col-lg-9">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                            <div class="alert">
                                <span class="tittle-alert entypo-info-circled"></span>
                                {!! $announcement !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="{{ route('showHome') }}">Accueil</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="{{ route('showVolunteerProfile') }}">Profil Volontaire</a>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->


            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
                <!-- PROFILE -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-lg-12 well profile">
                            <div class="col-sm-12">
                                <div class="col-xs-12 col-sm-4 text-center">

                                    <ul class="list-group">
                                        <li class="list-group-item text-left">
                                            <span class="entypo-user"></span> Informations Générales
                                        </li>
                                        <li class="list-group-item text-center">
                                            <img @if($volunteer->photo)src="{{ asset($volunteer->photo) }}"
                                                 @else src="{{ asset('img/unknown.png') }}" @endif alt=""
                                                 class="img-circle img-responsive img-profile">

                                        </li>

                                        <li class="list-group-item text-right">
                                            <span class="pull-left">
                                                <strong>Nom</strong>
                                            </span>{{ $volunteer->user->name }}</li>
                                        <li class="list-group-item text-right">
                                            <span class="pull-left">
                                                <strong>Email</strong>
                                            </span>{{ $volunteer->user->email }}</li>
                                        <li class="list-group-item text-right">
                                            <span class="pull-left">
                                                <strong>Téléphone</strong>
                                            </span>{{ $user->phone ? $user->phone : 'Aucun' }}</li>
                                        <li class="list-group-item text-right">
                                            <span class="pull-left">
                                                <strong>Bénévole Depuis</strong>
                                            </span>{{ $volunteer->user->created_at->format('d-m-Y') }}</li>
                                    </ul>

                                </div>
                                <div class="col-xs-12 col-sm-8 profile-name">
                                    <h2>Profil Bénévolat
                                    </h2>
                                    <hr>

                                        <dl class="dl-horizontal-profile">
                                            <dt>Disponiblé</dt>
                                            <dd class="pull-right">
                                                <div class="make-switch" data-on="primary" data-off="danger">
                                                    <input type="checkbox" @if($volunteer->status == 1) checked @endif disabled>
                                                </div>
                                            </dd>

                                            <dt>Organisation</dt>
                                            <dd>{{ $volunteer->organisation }}</dd>

                                            <dt>Age</dt>
                                            <dd>{{ $volunteer->age }}</dd>


                                            <dt>Téléphone</dt>
                                            <dd>{{ $volunteer->phone }}</dd>


                                            <dt>Sexe</dt>
                                            <dd>
                                                @if($volunteer->gender == 1) Homme @else Femme @endif
                                            </dd>

                                            <dt>Disponibilité</dt>
                                            <br>

                                                <div class="skin skin-flat row col-lg-12">
                                                    <div class="col-lg-3">
                                                        <input @if($volunteer->days != null and json_decode($volunteer->days)->d1 == 1) checked @endif disabled autocomplete="off" type="checkbox">
                                                        <label for="flat-checkbox-1" style="margin-left: 30px;">Lundi</label>
                                                    </div>

                                                    <div class="col-lg-3">
                                                        <input @if($volunteer->days != null and json_decode($volunteer->days)->d2 == 1) checked @endif disabled autocomplete="off" type="checkbox">
                                                        <label for="flat-checkbox-2" style="margin-left: 30px;">Mardi</label>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <input @if($volunteer->days != null and json_decode($volunteer->days)->d3 == 1) checked @endif disabled autocomplete="off" type="checkbox">
                                                        <label for="flat-checkbox-2" style="margin-left: 30px;">Mercredi</label>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <input @if($volunteer->days != null and json_decode($volunteer->days)->d4 == 1) checked @endif disabled autocomplete="off" type="checkbox">
                                                        <label for="flat-checkbox-2" style="margin-left: 30px;">Jeudi</label>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <input @if($volunteer->days != null and json_decode($volunteer->days)->d5 == 1) checked @endif disabled autocomplete="off" type="checkbox">
                                                        <label for="flat-checkbox-2" style="margin-left: 30px;">Vendredi</label>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <input @if($volunteer->days != null and json_decode($volunteer->days)->d6 == 1) checked @endif disabled autocomplete="off" type="checkbox">
                                                        <label for="flat-checkbox-2" style="margin-left: 30px;">Samedi</label>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <input @if($volunteer->days != null and json_decode($volunteer->days)->d7 == 1) checked @endif disabled autocomplete="off" type="checkbox">
                                                        <label for="flat-checkbox-2" style="margin-left: 30px;">Dimanche</label>
                                                    </div>


                                                </div>

                                            <br>
                                            <dt>Je peux </dt>
                                            <br>

                                            <div class="skin skin-flat row col-lg-12" style="margin-bottom:10px;">
                                                @foreach($categories as $category)
                                                <div class="col-lg-4">
                                                    <input @if($volunteer->categories->contains($category->id)) checked @endif type="checkbox">
                                                    <label for="flat-checkbox-1" style="margin-left: 30px;">{{ $category->title }}</label>
                                                </div>
                                                @endforeach

                                            </div>

                                            <br>

                                            <div id="map" style="height: 400px; width: 100%; margin-bottom: 10px"></div>

                                            <div class="pull-right" style=" margin-bottom: 10px">
                                                <button type="submit" class="btn btn-primary">Enregistrer</button>
                                            </div>
                                        </dl>
                                    </form>


                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <!-- END OF PROFILE -->


                <!-- /END OF CONTENT -->


                <!-- FOOTER -->
                <div class="footer-space"></div>
                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate"></p>
                        <p id="clock"></p>
                    </div>
                    <div class="copyright">Dévéloppé avec
                        <span class="entypo-heart"></span> par <a href="https://www.bluepenlabs.com" target="_blank">BluePen Labs</a>
                    </div>
                    <div class="devider-footer"></div>

                </div>
                <!-- / END OF FOOTER -->


            </div>

        </div>
    </div>

@endsection


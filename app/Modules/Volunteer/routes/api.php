<?php

Route::group(['module' => 'Volunteer', 'middleware' => ['api'], 'namespace' => 'App\Modules\Volunteer\Controllers'], function() {

    Route::resource('Volunteer', 'VolunteerController');

});

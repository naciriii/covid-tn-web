<?php

Route::group(['module' => 'Volunteer', 'middleware' => ['web'], 'namespace' => 'App\Modules\Volunteer\Controllers'], function() {

    Route::get('/volunteer/profile/update', 'WebController@showVolunteerUpdateProfile')->name('showVolunteerUpdateProfile');
    Route::post('/volunteer/profile/update', 'WebController@handleUpdateVolunteerProfile')->name('handleUpdateVolunteerProfile');

    Route::get('/volunteer/view/{id}', 'WebController@showVolunteerPublicProfile')->name('showVolunteerPublicProfile');

});

<?php

namespace App\Modules\Volunteer\Controllers;

use App\Modules\General\Models\City;
use App\Modules\General\Models\General;
use App\Modules\User\Models\User;
use App\Modules\Volunteer\Models\Category;
use App\Modules\Volunteer\Models\Volunteer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Kamaln7\Toastr\Facades\Toastr;

class WebController extends Controller
{

    public function showVolunteerUpdateProfile(){
        return view('Volunteer::frontOffice.private.profile', [
            'announcement' => General::find(1)->announcement,
            'categories' => Category::all(),
        ]);
    }

    public function handleUpdateVolunteerProfile(Request $request){
        $data = $request->all();

        $days = new \stdClass();
        $days->d1 = in_array(1, $data['days']) ? 1 : 0;
        $days->d2 = in_array(2, $data['days']) ? 1 : 0;
        $days->d3 = in_array(3, $data['days']) ? 1 : 0;
        $days->d4 = in_array(4, $data['days']) ? 1 : 0;
        $days->d5 = in_array(5, $data['days']) ? 1 : 0;
        $days->d6 = in_array(6, $data['days']) ? 1 : 0;
        $days->d7 = in_array(7, $data['days']) ? 1 : 0;

        if(Auth::user()->volunteer){
            $volunteer = Volunteer::find(Auth::user()->volunteer->id);

            $volunteer->status = isset($data['availability']) ? 1 : 0;
            $volunteer->organisation = $data['organisation'];
            $volunteer->age = $data['age'];
            $volunteer->gender = $data['gender'];
            $volunteer->days = json_encode($days);
            $volunteer->lat = $data['lat'];
            $volunteer->lng = $data['lng'];

                Volunteer::find($volunteer->id)->categories()->detach();

                $volunteer->save();
        } else {
            $volunteer = Volunteer::create([
                'status' => isset($data['availability']) ? 1 : 0,
                'organisation' =>  $data['organisation'],
                'age' => $data['age'],
                'user_id' => Auth::user()->id,
                'gender' => $data['gender'],
                'lng' => $data['lng'],
                'lat' => $data['lat'],
                'days' => json_encode($days),
            ]);
        }

        if(Auth::user()->phone == null and $data['phone'] != null){
            $user = User::find(Auth::id());
            $user->phone = $data['phone'];
            $user->save();
        }

        foreach($data['categories'] as $category){
            Volunteer::find($volunteer->id)->categories()->attach($category);
        }

        Toastr::success('Mise à jour effectuée avec succès !');
        return back();
    }

    public function showVolunteerPublicProfile($id){
        return view('Volunteer::frontOffice.public.single', [
            'announcement' => General::find(1)->announcement,
            'volunteer' => Volunteer::find($id),
            'categories' => Category::all(),
        ]);
    }
}

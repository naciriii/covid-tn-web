<?php

namespace App\Modules\Volunteer\Models;

use Illuminate\Database\Eloquent\Model;

class VolunteerCategory extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'volunteer_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'volunteer_id',
        'category_id',
        'details',
    ];

    public function volunteer()
    {
        return $this->hasOne('App\Modules\Volunteer\Models\Volunteer', 'id', 'volunteer_id');
    }

    public function category()
    {
        return $this->hasOne('App\Modules\Volunteer\Models\Category', 'id', 'category_id');
    }
}

<?php

namespace App\Modules\Volunteer\Models;

use Illuminate\Database\Eloquent\Model;

class Volunteer extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'volunteers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'age',
        'gender',
        'organisation',
        'lat',
        'lng',
        'days',
        'status',
    ];

    public function user()
    {
        return $this->hasOne('App\Modules\User\Models\User', 'id', 'user_id');
    }

    public function categories()
    {
        return $this->belongsToMany(
            'App\Modules\Volunteer\Models\Category',
            'volunteer_categories',
            'volunteer_id',
            'category_id'
        )->withTimestamps();
    }
}

<?php

Route::group(['module' => 'Hospital', 'middleware' => ['web'], 'namespace' => 'App\Modules\Hospital\Controllers'], function() {

    Route::get('/manager/hospitals', 'WebController@showManagerHospitalsList')->name('showManagerHospitalsList')->middleware('checkManagerAccess');
    Route::get('/manager/hospitals/needs', 'WebController@showManagerRequestTypesList')->name('showManagerRequestTypesList')->middleware('checkManagerAccess');

    Route::get('/manager/hospitals/needs/delete/{id}', 'WebController@handleManagerDeleteRequestType')->name('handleManagerDeleteRequestType')->middleware('checkManagerAccess');
    Route::post('/manager/hospitals/needs/edit/{id}', 'WebController@handleManagerEditRequestType')->name('handleManagerEditRequestType')->middleware('checkManagerAccess');
    Route::post('/manager/hospitals/needs/add/type', 'WebController@handleManagerAddRequestType')->name('handleManagerAddRequestType')->middleware('checkManagerAccess');
    Route::post('/manager/hospitals/needs/add/request', 'WebController@handleManagerAddRequest')->name('handleManagerAddRequest')->middleware('checkManagerAccess');

    Route::post('/manager/hospitals/add', 'WebController@handleManagerAddHospital')->name('handleManagerAddHospital')->middleware('checkManagerAccess');
    Route::post('/manager/hospitals/affect', 'WebController@handleManagerAffectHospital')->name('handleManagerAffectHospital')->middleware('checkManagerAccess');
    Route::post('/manager/hospitals/edit/{id}', 'WebController@handleManagerEditHospital')->name('handleManagerEditHospital')->middleware('checkManagerAccess');
    Route::get('/manager/hospitals/delete/{id}', 'WebController@handleManagerDeleteHospital')->name('handleManagerDeleteHospital')->middleware('checkManagerAccess');

    Route::post('/needs/add/entry', 'WebController@handleUserAddRequestEntry')->name('handleUserAddRequestEntry')->middleware('checkMedicalAccess');

    Route::get('/medical/hospital/{id}', 'WebController@showMedicalAgentHospital')->name('showMedicalAgentHospital')->middleware('checkMedicalAccess');
    Route::post('/medical/hospital/{id}/edit/', 'WebController@handleMedicalAgentEditHospital')->name('handleMedicalAgentEditHospital')->middleware('checkMedicalAccess');
    Route::post('/medical/hospital/{id}/needs/add', 'WebController@handleMedicalAgentAddRequest')->name('handleMedicalAgentAddRequest')->middleware('checkMedicalAccess');

    Route::get('/medical/hospital/{id}/needs', 'WebController@showMedicalAgentHospitalRequests')->name('showMedicalAgentHospitalRequests')->middleware('checkMedicalAccess');
    Route::get('/hospital/{id}', 'WebController@showSingleHospital')->name('showSingleHospital');

});

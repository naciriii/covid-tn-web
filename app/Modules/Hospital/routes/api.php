<?php

Route::group(['module' => 'Hospital', 'middleware' => ['api'], 'namespace' => 'App\Modules\Hospital\Controllers'], function() {

    Route::resource('Hospital', 'HospitalController');

});

<?php

if (!function_exists('transformHospitalType')) {
    /**
     * @param integer $id
     * @return string
     */
    function transformHospitalType($id)
    {
        switch ($id) {
            case 1 :
                return 'Publique';
            case 2 :
                return 'Privé';
            case 3 :
                return 'Forces de l\'ordre';
            case 4 :
                return 'Universitaire';
            default :
                return '';
        }
    }
}

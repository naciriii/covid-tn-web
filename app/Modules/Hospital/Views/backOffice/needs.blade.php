@extends('frontOffice.layout')

@section('head')
    @include('frontOffice.inc.head',
    ['title' => config('app.name') . ' - Liste des Hôpitaux',
    'description' => 'Liste des Hôpitaux - ' . config('app.name')
    ])
@endsection

@section('header')
    @include('frontOffice.inc.header')
@endsection

@section('sidebar-left')
    @include('frontOffice.inc.sidebar.left', [
        'current' => 'hospitals'
    ])
@endsection

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatable/datatable.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/datatable/datatable.js') }}"></script>

    <link href="{{ asset('plugins/bootstrap/switch.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('plugins/bootstrap/switch.js') }}"></script>

    <script type="text/javascript">

        /* Datatables responsive */

        $(document).ready(function () {
            $('#datatable-responsive').DataTable({
                responsive: true,
                language: {
                    url: "{{ asset('plugins/datatable/lang/fr.json') }}"
                }
            });
            $('#datatable-responsive-2').DataTable({
                responsive: true,
                language: {
                    url: "{{ asset('plugins/datatable/lang/fr.json') }}"
                }
            });
            $('.dataTables_filter input').attr("placeholder", "Rechercher...");
        });

    </script>

    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i>
                            <span>Annonces
                            </span>
                        </h2>

                    </div>

                    <div class="col-lg-9">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                            <div class="alert">
                                <span class="tittle-alert entypo-info-circled"></span>
                                {!! $announcement !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="{{ route('showHome') }}">Accueil</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="{{ route('showManagerRequestTypesList') }}">Besoins</a>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->


            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
                <div class="row">


                    <div class="col-sm-12">

                        <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <h6>
                                    Liste des Types de Besoins</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#FootableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#Footable">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="Footable">

                                <table id="datatable-responsive"
                                       class="display table table-striped table-bordered" cellspacing="0"
                                       width="100%">
                                    <thead>
                                    <tr>
                                        <th>Titre</th>
                                        <th>Min</th>
                                        <th>Max</th>
                                        <th>Usage</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tfoot>
                                    <tr>
                                        <th>Titre</th>
                                        <th>Min</th>
                                        <th>Max</th>
                                        <th>Usage</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>

                                    <tbody>
                                    @foreach($types as $type)
                                        <tr>
                                            <td>{{ $type->title }}</td>
                                            <td>{{ $type->min }}</td>
                                            <td>{{ $type->max }}</td>
                                            <td>{{ $type->hospitals->count() }}</td>
                                            <td style="text-align: center">
                                                <span class="badge badge-pill " title="Modifier" data-toggle="modal" data-target="#edit-{{ $type->id }}"><i class="fa fa-edit"></i></span>
                                                <a href="{{ route('handleManagerDeleteRequestType', ['id' => $type->id]) }}"><span class="badge badge-pill warning-color"><i class="fa fa-trash-o"></i></span></a>
                                            </td>
                                        </tr>
                                        <div class="modal fade" id="edit-{{ $type->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <form method="post" action="{{ route('handleManagerEditRequestType', ['id' => $type->id]) }}">
                                                        {{ csrf_field() }}
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Modifier {{ $type->title }}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <label class="control-label">Titre</label>
                                                                    <input name="title" required class="form-control" placeholder="Titre" value="{{ $type->title }}">
                                                                </div>

                                                                <div class="col-lg-6">
                                                                    <label class="control-label">Min</label>
                                                                    <input name="min" class="form-control" placeholder="" type="number" value="{{ $type->min }}">
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <label class="control-label">Max</label>
                                                                    <input name="max" class="form-control" placeholder="" type="number" value="{{ $type->max }}">
                                                                </div>

                                                            </div>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                                                            <button type="submit" class="btn btn-primary">Envoyer</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>

                    <div class="col-sm-8">

                        <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <h6>
                                    Nouvelle Entrée de Besoin</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#FootableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#Footable">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="Footable">

                                <form class="form-horizontal" role="form" action="{{ route('handleManagerAddRequest') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Hôpital:</label>
                                        <div class="col-lg-8">
                                        <select class="form-control" name="hospital_id">
                                            @foreach($hospitals as $hospital)
                                                <option value="{{ $hospital->id }}">{{ $hospital->title }}</option>
                                            @endforeach
                                        </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Type:</label>
                                        <div class="col-lg-8">
                                            <select class="form-control" name="type_id">
                                                @foreach($types as $type)
                                                    <option value="{{ $type->id }}">{{ $type->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Details:</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value=" " required name="details" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Renouvellable:</label>
                                        <div class="col-lg-8">
                                            <div class="make-switch" data-on="primary" data-off="info">
                                                <input type="checkbox" checked name="renew">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Renouvellement <sup>Jours</sup>:</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" required value="0" min="0" max="30" name="days" type="number">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Quantité:</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="1" min="1" required name="quantity" type="number">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Importance:</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" required value="1" name="severity" type="number" max="5" min="1">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div class="col-md-8">
                                            <input class="btn btn-primary" value="Ajouter" type="submit">
                                            <span></span>
                                            <input class="btn btn-default" value="Annuler" type="reset">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <h6>
                                    Liste des Demandes
                                </h6>
                                <div class="titleClose">
                                    <a class="gone" href="#FootableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#Footable">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="Footable">

                                <table id="datatable-responsive-2"
                                       class="display table table-striped table-bordered" cellspacing="0"
                                       width="100%">
                                    <thead>
                                    <tr>
                                        <th>Type</th>
                                        <th>Hôpital</th>
                                        <th>Renouvellable</th>
                                        <th>Quantité</th>
                                        <th>Reçu</th>
                                        <th>Importance</th>
                                    </tr>
                                    </thead>

                                    <tfoot>
                                    <tr>
                                        <th>Type</th>
                                        <th>Hôpital</th>
                                        <th>Renouvellable</th>
                                        <th>Quantité</th>
                                        <th>Reçu</th>
                                        <th>Importance</th>
                                    </tr>
                                    </tfoot>

                                    <tbody>
                                    @foreach($requests as $request)
                                        <tr>
                                            <td>{{ $request->type->title }}</td>
                                            <td>{{ $request->hospital->title }}</td>
                                            <td>{{ $request->auto_renew == 1 ? 'Oui' : 'Non'}}</td>
                                            <td>{{ $request->quantity }}</td>
                                            <td>{{ count($request->entries) > 0 ? $request->entries->sum('quantity') : 0 }}</td>
                                            <td>{{ $request->severity }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>

                    <div class="col-sm-4">

                        <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <h6>
                                    Nouvelle Entrée de Réception</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#FootableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#Footable">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="Footable">

                                <form class="form-horizontal" role="form" action="{{ route('handleUserAddRequestEntry') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Demande:</label>
                                        <div class="col-lg-8">
                                            <select class="form-control" name="request_id">
                                                @foreach($requests as $request)
                                                    @if(count($request->entries) == 0 or $request->entries->sum('quantity') < $request->quantity)
                                                    <option value="{{ $request->id }}">{{ $request->type->title }} - {{ $request->hospital->title }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Quantité:</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="" name="quantity" type="number" step="0.1">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div class="col-md-8">
                                            <input class="btn btn-primary" value="Ajouter" type="submit">
                                            <span></span>
                                            <input class="btn btn-default" value="Annuler" type="reset">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <h6>
                                    Nouvelle Entrée de Type</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#FootableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#Footable">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="Footable">

                                <form class="form-horizontal" role="form" action="{{ route('handleManagerAddRequestType') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Titre:</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="" required name="title" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Minimum:</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="" name="min" type="number" step="0.1">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Maximum:</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="" name="max" type="number" step="0.1">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div class="col-md-8">
                                            <input class="btn btn-primary" value="Ajouter" type="submit">
                                            <span></span>
                                            <input class="btn btn-default" value="Annuler" type="reset">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
            </div>            <!--  / DEVICE MANAGER -->


            <div class="content-wrap">
                <!-- FOOTER -->
                <div class="footer-space"></div>
                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate"></p>
                        <p id="clock"></p>
                    </div>
                    <div class="copyright">Dévéloppé avec
                        <span class="entypo-heart"></span> par <a href="https://www.bluepenlabs.com" target="_blank">BluePen Labs</a>
                    </div>
                    <div class="devider-footer"></div>


                </div>
                <!-- / END OF FOOTER -->


            </div>
        </div>
    </div>

@endsection

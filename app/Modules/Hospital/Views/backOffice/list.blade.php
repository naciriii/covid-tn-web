@extends('frontOffice.layout')

@section('head')
    @include('frontOffice.inc.head',
    ['title' => config('app.name') . ' - Liste des Hôpitaux',
    'description' => 'Liste des Hôpitaux - ' . config('app.name')
    ])
@endsection

@section('header')
    @include('frontOffice.inc.header')
@endsection

@section('sidebar-left')
    @include('frontOffice.inc.sidebar.left', [
        'current' => 'hospitals'
    ])
@endsection

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatable/datatable.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/datatable/datatable.js') }}"></script>

    <script type="text/javascript">

        /* Datatables responsive */

        $(document).ready(function () {
            $('#datatable-responsive').DataTable({
                responsive: true,
                language: {
                    url: "{{ asset('plugins/datatable/lang/fr.json') }}"
                }
            });
            $('.dataTables_filter input').attr("placeholder", "Rechercher...");
        });

    </script>

    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i>
                            <span>Annonces
                            </span>
                        </h2>

                    </div>

                    <div class="col-lg-9">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                            <div class="alert">
                                <span class="tittle-alert entypo-info-circled"></span>
                                {!! $announcement !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="{{ route('showHome') }}">Accueil</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="{{ route('showManagerHospitalsList') }}">Hôpitaux</a>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->


            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
                <div class="row">


                    <div class="col-sm-12">

                        <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <h6>
                                    Liste des Hôpitaux</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#FootableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#Footable">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="Footable">

                                <table id="datatable-responsive"
                                       class="display table table-striped table-bordered" cellspacing="0"
                                       width="100%">
                                    <thead>
                                    <tr>
                                        <th>Titre</th>
                                        <th>Type</th>
                                        <th>Gouvernorat</th>
                                        <th>Chargé</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tfoot>
                                    <tr>
                                        <th>Titre</th>
                                        <th>Type</th>
                                        <th>Gouvernorat</th>
                                        <th>Chargé</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>

                                    <tbody>
                                    @foreach($hospitals as $hospital)
                                        <tr>
                                            <td>{{ $hospital->title }}</td>
                                            <td>{{ transformHospitalType($hospital->type) }}</td>
                                            <td>{{ $hospital->city->title }}</td>
                                            <td>{{ $hospital->user ? $hospital->user->name : 'Aucun' }}</td>
                                            <td style="text-align: center">
                                                <span class="badge badge-pill " title="Modifier" data-toggle="modal" data-target="#edit-{{ $hospital->id }}"><i class="fa fa-edit"></i></span>
                                                <a href="{{ route('handleManagerDeleteHospital', ['id' => $hospital->id]) }}"><span class="badge badge-pill warning-color"><i class="fa fa-trash-o"></i></span></a>
                                            </td>
                                        </tr>
                                        <div class="modal fade" id="edit-{{ $hospital->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <form method="post" action="{{ route('handleManagerEditHospital', ['id' => $hospital->id]) }}">
                                                        {{ csrf_field() }}
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Modifier {{ $hospital->title }}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <label class="control-label">Titre</label>
                                                                    <input name="title" required class="form-control" placeholder="Titre" value="{{ $hospital->title }}">
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <label class="control-label">Type</label>
                                                                    <select class="form-control" name="type" autocomplete="off">
                                                                        <option @if($hospital->type == 1) selected @endif value="1">Publique</option>
                                                                        <option @if($hospital->type == 2) selected @endif value="2">Privé</option>
                                                                        <option @if($hospital->type == 3) selected @endif value="3">Forces de l'ordre</option>
                                                                        <option @if($hospital->type == 4) selected @endif value="4">Universitaire</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <label class="control-label">Gouvernorat</label>
                                                                    <select class="form-control" name="city" autocomplete="off">
                                                                        @foreach($cities as $city)
                                                                            <option @if($hospital->city_id == $city->id) selected @endif value="{{ $city->id }}">{{ $city->title }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <label class="control-label">Utilisateur</label>
                                                                    <select class="form-control" name="user" autocomplete="off">
                                                                        <option>Aucun</option>
                                                                        @foreach($users as $user)
                                                                            <option @if($hospital->user_id == $user->id) selected @endif value="{{ $user->id }}">{{ $user->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <label class="control-label">Adresse</label>
                                                                    <input name="address" class="form-control" placeholder="" value="{{ $hospital->address }}">
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <label class="control-label">Latitude</label>
                                                                    <input name="lat" required class="form-control" placeholder="" value="{{ $hospital->lat }}">
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <label class="control-label">Longitude</label>
                                                                    <input name="lng" required class="form-control" placeholder="" value="{{ $hospital->lng }}">
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <label class="control-label">Téléphone</label>
                                                                    <input name="phone" class="form-control" placeholder="" value="{{ $hospital->phone }}" type="number">
                                                                </div>

                                                                <div class="col-lg-6">
                                                                    <label class="control-label">IBAN</label>
                                                                    <input name="iban" class="form-control" placeholder="" value="{{ $hospital->iban }}" >
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <label class="control-label">RIB</label>
                                                                    <input name="rib" class="form-control" type="number" value="{{ $hospital->rib }}">
                                                                </div>
                                                                <div class="col-lg-3">
                                                                    <label class="control-label">Banque</label>
                                                                    <input name="bank" class="form-control" placeholder="" value="{{ $hospital->bank }}">
                                                                </div>
                                                                <div class="col-lg-3">
                                                                    <label class="control-label">Bic/Swift</label>
                                                                    <input name="bs" class="form-control" placeholder="" value="{{ $hospital->bs }}">
                                                                </div>

                                                            </div>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                                                            <button type="submit" class="btn btn-primary">Envoyer</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>

                    <div class="col-sm-6">

                        <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <h6>
                                    Nouvelle Entrée</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#FootableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#Footable">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="Footable">

                                <form class="form-horizontal" role="form" action="{{ route('handleManagerAddHospital') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Titre:</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="" required name="title" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Type:</label>
                                        <div class="col-lg-8">
                                            <div class="ui-select">
                                                <select class="form-control" name="type">
                                                    <option value="1">Publique</option>
                                                    <option value="2">Privé</option>
                                                    <option value="3">Forces de l'ordre</option>
                                                    <option value="4">Universitaire</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Gouvernorat:</label>
                                        <div class="col-lg-8">
                                            <div class="ui-select">
                                                <select class="form-control" name="city">
                                                    @foreach($cities as $city)
                                                    <option value="{{ $city->id }}">{{ $city->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Utilisateur:</label>
                                        <div class="col-lg-8">
                                            <div class="ui-select">
                                                <select class="form-control" name="user">
                                                    <option>Aucun</option>
                                                @foreach($users as $user)
                                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Adresse :</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="" name="address" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Latitude:</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="" name="lat" type="text" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Longitude:</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="" name="lng" type="text" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Téléphone :</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="" name="phone" type="number">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">IBAN :</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="" name="iban" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">RIB :</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="" name="rib" type="number">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Banque :</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="" name="bank" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">BIC/Swift :</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="" name="bs" type="text">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div class="col-md-8">
                                            <input class="btn btn-primary" value="Ajouter" type="submit">
                                            <span></span>
                                            <input class="btn btn-default" value="Annuler" type="reset">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>


                    </div>

                    <div class="col-sm-6">

                        <div class="nest" id="Footable2Close">
                            <div class="title-alt">
                                <h6>
                                    Affectation aux Hôpitaux</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#Footable2Close">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#Footable2">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="Footable2">

                                <form class="form-horizontal" role="form" action="{{ route('handleManagerAffectHospital') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Hôpital:</label>
                                        <div class="col-lg-8">
                                            <div class="ui-select">
                                                <select class="form-control" name="hospital_id">
                                                    @foreach($hospitals->where('user_id', null) as $hospital)
                                                        <option value="{{ $hospital->id }}">{{ $hospital->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Utilisateur:</label>
                                        <div class="col-lg-8">
                                            <div class="ui-select">
                                                <select class="form-control" name="user_id">
                                                    @foreach($users as $user)
                                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div class="col-md-8">
                                            <input class="btn btn-primary" value="Ajouter" type="submit">
                                            <span></span>
                                            <input class="btn btn-default" value="Annuler" type="reset">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>


                    </div>

                </div>
            </div>            <!--  / DEVICE MANAGER -->


            <div class="content-wrap">
                <!-- FOOTER -->
                <div class="footer-space"></div>
                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate"></p>
                        <p id="clock"></p>
                    </div>
                    <div class="copyright">Dévéloppé avec
                        <span class="entypo-heart"></span> par <a href="https://www.bluepenlabs.com" target="_blank">BluePen Labs</a>
                    </div>
                    <div class="devider-footer"></div>


                </div>
                <!-- / END OF FOOTER -->


            </div>
        </div>
    </div>

@endsection

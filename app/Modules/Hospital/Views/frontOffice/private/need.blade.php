@extends('frontOffice.layout')

@section('head')
    @include('frontOffice.inc.head',
    ['title' => config('app.name') . ' - Besoins de ' . $hospital->title,
    'description' => 'Besoins de ' . $hospital->title . ' - ' . config('app.name')
    ])
@endsection

@section('header')
    @include('frontOffice.inc.header')
@endsection

@section('sidebar-left')
    @include('frontOffice.inc.sidebar.left', [
        'current' => 'need'
    ])
@endsection

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatable/datatable.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/datatable/datatable.js') }}"></script>

    <link href="{{ asset('plugins/bootstrap/switch.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('plugins/bootstrap/switch.js') }}"></script>

    <script type="text/javascript">

        /* Datatables responsive */

        $(document).ready(function () {
            $('#datatable-responsive').DataTable({
                responsive: true,
                language: {
                    url: "{{ asset('plugins/datatable/lang/fr.json') }}"
                }
            });
            $('#datatable-responsive-2').DataTable({
                responsive: true,
                language: {
                    url: "{{ asset('plugins/datatable/lang/fr.json') }}"
                }
            });
            $('.dataTables_filter input').attr("placeholder", "Rechercher...");
        });

    </script>

    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i>
                            <span>Annonces
                            </span>
                        </h2>

                    </div>

                    <div class="col-lg-9">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                            <div class="alert">
                                <span class="tittle-alert entypo-info-circled"></span>
                                {!! $announcement !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="{{ route('showHome') }}">Accueil</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="{{ route('showManagerRequestTypesList') }}">Besoins</a>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->


            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
                <div class="row">


                    <div class="col-sm-12">

                        <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <h6>
                                    Liste des Besoins</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#FootableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#Footable">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="Footable">

                                <table id="datatable-responsive-2"
                                       class="display table table-striped table-bordered" cellspacing="0"
                                       width="100%">
                                    <thead>
                                    <tr>
                                        <th>Type</th>
                                        <th>Renouvellable</th>
                                        <th>Quantité</th>
                                        <th>Reçu</th>
                                        <th>Importance</th>
                                        <th>Date</th>
                                    </tr>
                                    </thead>

                                    <tfoot>
                                    <tr>
                                        <th>Type</th>
                                        <th>Renouvellable</th>
                                        <th>Quantité</th>
                                        <th>Reçu</th>
                                        <th>Importance</th>
                                        <th>Date</th>
                                    </tr>
                                    </tfoot>

                                    <tbody>
                                    @foreach($requests as $request)
                                        <tr>
                                            <td>{{ $request->type->title }}</td>
                                            <td>{{ $request->auto_renew == 1 ? 'Oui' : 'Non' }}</td>
                                            <td>{{ $request->quantity }}</td>
                                            <td>{{ count($request->entries) > 0 ? $request->entries->sum('quantity') : 0 }}</td>
                                            <td>{{ $request->severity }}</td>
                                            <td>{{ $request->created_at->format('d-m-Y') }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>

                    <div class="col-sm-8">

                        <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <h6>
                                    Nouvelle Entrée de Besoin</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#FootableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#Footable">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="Footable">

                                <form class="form-horizontal" role="form" action="{{ route('handleMedicalAgentAddRequest', ['id' => $hospital->id ]) }}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="hospital_id" value="{{ $hospital->id }}">
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Type:</label>
                                        <div class="col-lg-8">
                                            <select class="form-control" name="type_id">
                                                @foreach($types as $type)
                                                    <option value="{{ $type->id }}">{{ $type->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Details:</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value=" " required name="details" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Renouvellable:</label>
                                        <div class="col-lg-8">
                                            <div class="make-switch" data-on="primary" data-off="warning">
                                                <input type="checkbox" checked name="renew">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Renouvellement <sup>Chaque X Jours</sup>:</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" required value="1" min="0" max="30" name="days" type="number">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Quantité:</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="1" min="1" required name="quantity" type="number">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Importance:</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" required value="1" name="severity" type="number" max="5" min="1">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div class="col-md-8">
                                            <input class="btn btn-primary" value="Ajouter" type="submit">
                                            <span></span>
                                            <input class="btn btn-default" value="Annuler" type="reset">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>


                    </div>

                    <div class="col-sm-4">

                        <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <h6>
                                    Nouvelle Entrée de Réception</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#FootableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#Footable">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="Footable">

                                <form class="form-horizontal" role="form" action="{{ route('handleUserAddRequestEntry') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Demande:</label>
                                        <div class="col-lg-8">
                                            <select class="form-control" name="request_id">
                                                @foreach($requests as $request)
                                                    @if(count($request->entries) == 0 or $request->entries->sum('quantity') < $request->quantity)
                                                     <option value="{{ $request->id }}">{{ $request->type->title }} - {{ $request->created_at->format('d-m-Y') }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Quantité:</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="" name="quantity" type="number" step="0.1">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div class="col-md-8">
                                            <input class="btn btn-primary" value="Ajouter" type="submit">
                                            <span></span>
                                            <input class="btn btn-default" value="Annuler" type="reset">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
            </div>            <!--  / DEVICE MANAGER -->


            <div class="content-wrap">
                <!-- FOOTER -->
                <div class="footer-space"></div>
                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate"></p>
                        <p id="clock"></p>
                    </div>
                    <div class="copyright">Dévéloppé avec
                        <span class="entypo-heart"></span> par <a href="https://www.bluepenlabs.com" target="_blank">BluePen Labs</a>
                    </div>
                    <div class="devider-footer"></div>


                </div>
                <!-- / END OF FOOTER -->


            </div>
        </div>
    </div>

@endsection

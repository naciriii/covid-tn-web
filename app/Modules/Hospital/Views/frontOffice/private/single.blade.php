@extends('frontOffice.layout')

@section('head')
    @include('frontOffice.inc.head',
    ['title' => config('app.name') . ' - ' . $hospital->title,
    'description' => $hospital->title . ' - ' . config('app.name')
    ])
@endsection

@section('header')
    @include('frontOffice.inc.header')
@endsection

@section('sidebar-left')
    @include('frontOffice.inc.sidebar.left', [
        'current' => 'hospital'
    ])
@endsection

@section('content')
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i>
                            <span>Annonces
                            </span>
                        </h2>

                    </div>

                    <div class="col-lg-9">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                            <div class="alert">
                                <span class="tittle-alert entypo-info-circled"></span>
                                {!! $announcement !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="{{ route('showHome') }}">Accueil</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="{{ route('showMedicalAgentHospital', ['id' => $hospital->id]) }}">{{ $hospital->title  }}</a>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->


            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
                <div class="row">


                    <div class="col-sm-12">

                        <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <h6>
                                    Mon Hôpital</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#FootableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#Footable">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="Footable">

                                <form class="form-horizontal" role="form" action="{{ route('handleMedicalAgentEditHospital', ['id' => $hospital->id]) }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Titre:</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="{{ $hospital->title }}" required name="title" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Type:</label>
                                        <div class="col-lg-8">
                                            <div class="ui-select">
                                                <select class="form-control" name="type">
                                                    <option @if($hospital->type == 1) selected @endif value="1">Publique</option>
                                                    <option @if($hospital->type == 2) selected @endif value="2">Privé</option>
                                                    <option @if($hospital->type == 3) selected @endif value="3">Forces de l'ordre</option>
                                                    <option @if($hospital->type == 4) selected @endif value="4">Universitaire</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Gouvernorat:</label>
                                        <div class="col-lg-8">
                                            <div class="ui-select">
                                                <select class="form-control" name="city">
                                                    @foreach($cities as $city)
                                                        <option @if($city->id == $hospital->city_id) selected @endif value="{{ $city->id }}">{{ $city->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Adresse :</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="{{ $hospital->address }}" name="address" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Latitude:</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="{{ $hospital->lat }}"  type="text" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Longitude:</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="{{ $hospital->lng }}" type="text" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Téléphone :</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="{{ $hospital->phone }}" name="phone" type="number">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">IBAN :</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="{{ $hospital->iban }}"  type="text" disabled="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">RIB :</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="{{ $hospital->rib }}" type="number" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Banque :</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="{{ $hospital->bank }}" type="text" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">BIC/Swift :</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="{{ $hospital->bs }}" type="text" disabled>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div class="col-md-8">
                                            <input class="btn btn-primary" value="Enregistrer" type="submit">
                                            <span></span>
                                            <input class="btn btn-default" value="Annuler" type="reset">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>



                    </div>


                </div>
            </div>            <!--  / DEVICE MANAGER -->


            <div class="content-wrap">
                <!-- FOOTER -->
                <div class="footer-space"></div>
                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate"></p>
                        <p id="clock"></p>
                    </div>
                    <div class="copyright">Dévéloppé avec
                        <span class="entypo-heart"></span> par <a href="https://www.bluepenlabs.com" target="_blank">BluePen Labs</a>
                    </div>
                    <div class="devider-footer"></div>


                </div>
                <!-- / END OF FOOTER -->


            </div>
        </div>
    </div>

@endsection

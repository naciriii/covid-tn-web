@extends('frontOffice.layout')

@section('head')
    @include('frontOffice.inc.head',
    ['title' => config('app.name') . ' - J\'AIDE',
    'description' => 'J\'AIDE - ' . config('app.name')
    ])
@endsection

@section('header')
    @include('frontOffice.inc.header')
@endsection

@section('sidebar-left')
    @include('frontOffice.inc.sidebar.left', [
        'current' => 'general'
    ])
@endsection

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatable/datatable.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/datatable/datatable.js') }}"></script>

    <script type="text/javascript">

        /* Datatables responsive */

        $(document).ready(function () {
            $('#datatable-responsive').DataTable({
                responsive: true,
                language: {
                    url: "{{ asset('plugins/datatable/lang/fr.json') }}"
                }
            });
            $('.dataTables_filter input').attr("placeholder", "Rechercher...");
        });

    </script>

    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i>
                            <span>Annonces
                            </span>
                        </h2>

                    </div>

                    <div class="col-lg-9">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                            <div class="alert">
                                <span class="tittle-alert entypo-info-circled"></span>
                                {!! $announcement !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="{{ route('showHome') }}">Accueil</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="{{ route('showSingleHospital', ['id' => $hospital->id]) }}">J'AIDE</a>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->


            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
                <!-- PROFILE -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-lg-12 well profile">
                            <div class="col-sm-12">
                                <div class="col-xs-12 col-sm-4 text-center">

                                    <ul class="list-group">
                                        <li class="list-group-item text-left">
                                            <span class="entypo-user"></span>&nbsp;&nbsp;Contact</li>
                                        <li class="list-group-item text-center">
                                            <img @if($hospital->user->photo)src="{{ asset($hospital->user->photo) }}" @else src="{{ asset('img/unknown.png') }}" @endif alt="" class="img-circle img-responsive img-profile">

                                        </li>

                                        <li class="list-group-item text-right">
                                            <span class="pull-left">
                                                <strong>Nom</strong>
                                            </span>{{ $hospital->user->name }}</li>
                                        <li class="list-group-item text-right">
                                            <span class="pull-left">
                                                <strong>Téléphone</strong>
                                            </span>{{ $hospital->user->phone ? $hospital->user->phone : 'Aucun' }}</li>
                                        <li class="list-group-item text-right">
                                            <span class="pull-left">
                                                <strong>Nous à Rejoint</strong>
                                            </span>{{ $hospital->user->created_at->format('d-m-Y') }}</li>
                                    </ul>

                                </div>
                                <div class="col-xs-12 col-sm-8 profile-name">
                                    <h2>{{ $hospital->title }}
                                    </h2>
                                    <hr>

                                    <dl class="dl-horizontal-profile">
                                        <dt>Adresse</dt>
                                        <dd>{{ $hospital->address }}</dd>

                                        <dt>Gouvernorat</dt>
                                        <dd>{{ $hospital->city->title }}</dd>

                                        <dt>Type</dt>
                                        <dd>{{ transformHospitalType($hospital->type) }}</dd>

                                        <dt>Téléphone</dt>
                                        <dd>{{ $hospital->phone }}</dd>

                                        <dt>RIB</dt>
                                        <dd>{{ $hospital->rib }}</dd>

                                        <dt>IBAN</dt>
                                        <dd>{{ $hospital->iban }}</dd>

                                        <dt>Banque</dt>
                                        <dd>{{ $hospital->bank }}</dd>

                                        <dt>BIC/Swift</dt>
                                        <dd>{{ $hospital->bs }}</dd>
                                    </dl>


                                    <hr>

                                    <h5>
                                        <span class="entypo-arrows-ccw"></span>&nbsp;&nbsp;Besoins</h5>

                                    <div class="table-responsive">
                                        <table class="table table-hover table-striped table-condensed">

                                            <tbody>
                                            @foreach($hospital->requests as $request)
                                            <tr>
                                                <td>@if(count($request->entries) > 0) {{ $request->entries->sum('quantity') }} @else 0 @endif <sup>Reçus</sup> <strong>{{ $request->quantity }}</strong> {{$request->type->title }}</td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <!-- END OF PROFILE -->


                <!-- /END OF CONTENT -->



                <!-- FOOTER -->
                <div class="footer-space"></div>
                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate"></p>
                        <p id="clock"></p>
                    </div>
                    <div class="copyright">Dévéloppé avec
                        <span class="entypo-heart"></span> par <a href="https://www.bluepenlabs.com" target="_blank">BluePen Labs</a></div>
                    <div class="devider-footer"></div>

                </div>
                <!-- / END OF FOOTER -->


            </div>

        </div>
    </div>

@endsection


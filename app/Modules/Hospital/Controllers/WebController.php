<?php

namespace App\Modules\Hospital\Controllers;

use App\Modules\General\Models\City;
use App\Modules\General\Models\General;
use App\Modules\Hospital\Models\Hospital;
use App\Modules\Hospital\Models\HospitalRequest;
use App\Modules\Hospital\Models\HospitalRequestEntry;
use App\Modules\Hospital\Models\RequestType;
use App\Modules\User\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Kamaln7\Toastr\Facades\Toastr;

class WebController extends Controller
{

    public static function scheduleRecurrentRequests(){
        $requests = HospitalRequest::where('auto_renew', '=', 1)
            ->where('renewal_days', '>', 0)
            ->where('status', '=', 1)
            ->get();

        foreach($requests as $request){
            if(now()->diffInDays($request->created_at) == $request->renewal_days){
                HospitalRequest::create([
                   'request_type_id' => $request->request_type_id,
                   'hospital_id' => $request->hospital_id,
                   'details' => $request->details,
                   'auto_renew' => $request->auto_renew,
                   'renewal_days' => $request->renewal_days,
                   'quantity' => $request->quantity,
                   'severity' => $request->severity,
                ]);

                $request->status = 2;
                $request->save();
            }
        }
    }

    public function showManagerHospitalsList(){
        return view('Hospital::backOffice.list', [
            'hospitals' => Hospital::all(),
            'cities' => City::all(),
            'users' => User::whereHas('roles', function ($query) {
                $query->where('roles.id', '=', 3);
            })->whereDoesntHave('hospital')->get(),
            'announcement' => General::find(1)->announcement
        ]);
    }

    public function handleManagerAddHospital(Request $request){
        $data = $request->all();

        Hospital::create([
            'title' => $data['title'],
            'type' => $data['type'],
            'city_id' => $data['city'],
            'address' => isset($data['address']) ? $data['address'] : null,
            'lat' => $data['lat'],
            'lng' => $data['lng'],
            'phone' => isset($data['phone']) ? $data['phone']: null,
            'iban' => isset($data['iban']) ? $data['iban']: null,
            'rib' => isset($data['rib']) ? $data['rib'] : null,
            'bank' => isset($data['bank']) ? $data['bank'] : null,
            'bic_swift' => isset($data['bs']) ? $data['bs'] : null,
        ]);

        Toastr::success('Hôpital ajouté avec succès !');
        return back();
    }

    public function handleManagerDeleteHospital($id){
        $hospital = Hospital::find($id);

        if(!$hospital){
            Toastr::error('Hôpital introuvable');
            return back();
        }

        HospitalRequest::where('hospital_id', $id)->delete();
        $hospital->delete();

        Toastr::warning('Hôpital supprimé');
        return back();

    }

    public function handleManagerAffectHospital(Request $request){
        $data = $request->all();

        $hospital = Hospital::find($data['hospital_id']);

        if(!$hospital){
            Toastr::error('Hôpital introuvable');
            return back();
        }

        $hospital->user_id = $data['user_id'];
        $hospital->save();

        Toastr::success('Hôpital rattaché');
        return back();

    }

    public function handleManagerEditHospital($id, Request $request){
        $data = $request->all();

        $hospital = Hospital::find($id);

        $hospital->title = isset($data['title']) ? $data['title'] : $hospital->title;
        $hospital->address = isset($data['address']) ? $data['address'] : $hospital->address;
        $hospital->city_id = $data['city'];
        $hospital->lat = isset($data['lat']) ? $data['lat'] : $hospital->lat;
        $hospital->lng = isset($data['lng']) ? $data['lng'] : $hospital->lng;
        $hospital->phone = isset($data['phone']) ? $data['phone'] : $hospital->phone;
        $hospital->iban = isset($data['iban']) ? $data['iban'] : $hospital->iban;
        $hospital->rib = isset($data['rib']) ? $data['rib'] : $hospital->rib;
        $hospital->bank = isset($data['bank']) ? $data['bank'] : $hospital->bank;
        $hospital->bic_swift = isset($data['bs']) ? $data['bs'] : $hospital->bic_swift;
        $hospital->user_id = isset($data['user']) ? $data['user'] : $hospital->user_id;

        $hospital->save();

        Toastr::success('Hôpital modifié');
        return back();
    }

    public function showManagerRequestTypesList(){

        return view('Hospital::backOffice.needs', [
            'hospitals' => Hospital::all(),
            'types' => RequestType::all(),
            'requests' => HospitalRequest::where('status', 1)->get(),
            'announcement' => General::find(1)->announcement
        ]);
    }

    public function handleManagerAddRequestType(Request $request){
        $data = $request->all();

        RequestType::create([
            'title' => $data['title'],
            'max' => isset($data['max']) ? $data['max'] : null,
            'min' => isset($data['min']) ? $data['min'] : null,
        ]);

        Toastr::success('Type ajouté avec succès !');
        return back();
    }


    public function handleManagerEditRequestType($id, Request $request){
        $data = $request->all();

        $type = RequestType::find($id);

        $type->title = isset($data['title']) ? $data['title'] : $type->title;
        $type->min = isset($data['min']) ? $data['min'] : $type->min;
        $type->max = isset($data['max']) ? $data['max'] : $type->max;

        $type->save();

        Toastr::success('Type modifié');
        return back();
    }

    public function handleManagerDeleteRequestType($id){
        $type = RequestType::find($id);

        if(!$type){
            Toastr::error('Type introuvable');
            return back();
        }


        $requests = HospitalRequest::where('request_type_id', $id)->get();
        foreach($requests as $request){
            HospitalRequestEntry::where('request_id', $request->id)->delete();
        }
        HospitalRequest::where('request_type_id', $id)->delete();
        $type->delete();

        Toastr::warning('Type supprimé');
        return back();
    }

    public function handleManagerAddRequest(Request $request){
        $data = $request->all();

        HospitalRequest::create([
            'hospital_id' => $data['hospital_id'],
            'request_type_id' => $data['type_id'],
            'details' => isset($data['details']) ? $data['details'] : null,
            'auto_renew' => isset($data['renew']) ? 1 : 0,
            'renewal_days' => $data['days'],
            'quantity' => $data['quantity'],
            'severity' => $data['severity']
        ]);

        Toastr::success('Besoin ajouté avec succès !');
        return back();
    }

    public function handleUserAddRequestEntry(Request $request){
        $data = $request->all();

        $hospitalRequest = HospitalRequest::where('id', $data['request_id'])->first();
        if(count($hospitalRequest->entries) > 0){
            $max = $hospitalRequest->quantity - $hospitalRequest->entries->sum('quantity');
        } else {
            $max = $hospitalRequest->quantity;
        }

        if($max < $data['quantity']){
            Toastr::error('La quantité entrée est supérieure a celle demandée !');
            return back();
        }

        HospitalRequestEntry::create([
            'request_id' => $data['request_id'],
            'user_id' => Auth::user()->id,
            'quantity' => $data['quantity'],
        ]);

        Toastr::success('Besoin mis à jour avec succès !');
        return back();
    }

    public function showMedicalAgentHospital($id){
        $hospital = Hospital::find($id);

        if (!Auth::user() or $hospital->user_id != Auth::user()->id){
            Toastr::error('Vous n\'êtes pas authorisé a accéder à cette page !');
            return back();
        }
        return view('Hospital::frontOffice.private.single', [
            'hospital' => Hospital::find($id),
            'types' => RequestType::all(),
            'cities' => City::all(),
            'announcement' => General::find(1)->announcement
        ]);
    }

    public function handleMedicalAgentEditHospital($id, Request $request){
        $hospital = Hospital::find($id);

        if($hospital->user_id != Auth::user()->id){
            Toastr::error('Hôpital non relié a votre profil !');
            return back();
        }

        $data = $request->all();

        $hospital->title = $data['title'];
        $hospital->type = $data['type'];
        $hospital->phone = $data['phone'];
        $hospital->city_id = $data['city'];
        $hospital->address = $data['address'];

        $hospital->save();

        Toastr::success('Hôpital mis à jour avec succès !');
        return back();
    }

    public function showMedicalAgentHospitalRequests(){
        $hospital = Hospital::where('user_id', Auth::user()->id)->first();

        if(!$hospital){
            Toastr::error('Hôpital introuvable !');
            return back();
        }

        return view('Hospital::frontOffice.private.need', [
            'hospital' => $hospital,
            'types' => RequestType::all(),
            'requests' => HospitalRequest::where('hospital_id', $hospital->id)->get(),
            'cities' => City::all(),
            'announcement' => General::find(1)->announcement
        ]);
    }

    public function handleMedicalAgentAddRequest(Request $request){
        $data = $request->all();

        HospitalRequest::create([
            'hospital_id' => $data['hospital_id'],
            'request_type_id' => $data['type_id'],
            'details' => isset($data['details']) ? $data['details'] : null,
            'auto_renew' => isset($data['renew']) ? 1 : 0,
            'renewal_days' => $data['days'],
            'quantity' => $data['quantity'],
            'severity' => $data['severity']
        ]);

        Toastr::success('Besoin ajouté avec succès !');
        return back();
    }

    public function showSingleHospital($id){
        return view('Hospital::frontOffice.public.help', [
            'hospital' => Hospital::find($id),
            'announcement' => General::find(1)->announcement
        ]);
    }
}

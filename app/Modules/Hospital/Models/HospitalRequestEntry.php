<?php

namespace App\Modules\Hospital\Models;

use Illuminate\Database\Eloquent\Model;

class HospitalRequestEntry extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hospital_requests_entries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'request_id',
        'user_id',
        'quantity',
    ];

    public function request()
    {
        return $this->hasOne('App\Modules\Hospital\Models\HospitalRequest', 'id', 'request_id');
    }

    public function user()
    {
        return $this->hasOne('App\Modules\User\Models\User', 'id', 'user_id');
    }

}

<?php

namespace App\Modules\Hospital\Models;

use Illuminate\Database\Eloquent\Model;

class HospitalRequest extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hospital_requests';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'request_type_id',
        'hospital_id',
        'details',
        'auto_renew',
        'renewal_days',
        'quantity',
        'severity',
        'status',
    ];

    public function user()
    {
        return $this->hasOne('App\Modules\User\Models\User', 'id', 'user_id');
    }

    public function type()
    {
        return $this->hasOne('App\Modules\Hospital\Models\RequestType', 'id', 'request_type_id');
    }

    public function hospital()
    {
        return $this->hasOne('App\Modules\Hospital\Models\Hospital', 'id', 'hospital_id');
    }

    public function entries()
    {
        return $this->hasMany('App\Modules\Hospital\Models\HospitalRequestEntry', 'request_id', 'id');
    }

}

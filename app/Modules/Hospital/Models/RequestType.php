<?php

namespace App\Modules\Hospital\Models;

use Illuminate\Database\Eloquent\Model;

class RequestType extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'requests_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'min',
        'max',
    ];

    public function hospitals()
    {
        return $this->hasMany('App\Modules\Hospital\Models\HospitalRequest', 'request_type_id', 'id');
    }

}

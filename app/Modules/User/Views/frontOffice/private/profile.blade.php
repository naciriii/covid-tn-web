@extends('frontOffice.layout')

@section('head')
    @include('frontOffice.inc.head',
    ['title' => config('app.name') . ' - Mon Profil',
    'description' => 'Mon Profil- ' . config('app.name')
    ])
@endsection

@section('header')
    @include('frontOffice.inc.header')
@endsection

@section('sidebar-left')
    @include('frontOffice.inc.sidebar.left', [
        'current' => 'profile'
    ])
@endsection

@section('content')
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i>
                            <span>Annonces
                            </span>
                        </h2>

                    </div>

                    <div class="col-lg-9">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                            <div class="alert">
                                <span class="tittle-alert entypo-info-circled"></span>
                                {!! $announcement !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="{{ route('showHome') }}">Accueil</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="{{ route('showUserProfile') }}">Mon Profil</a>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->


            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
                <!-- PROFILE -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-lg-12 well profile">
                            <div class="col-sm-12">
                                <div class="col-xs-12 col-sm-4 text-center">

                                    <ul class="list-group">
                                        <li class="list-group-item text-left">
                                            <span class="entypo-user"></span> Informations Générales</li>
                                        <li class="list-group-item text-center">
                                            <img @if(Auth::user()->photo)src="{{ asset(Auth::user()->photo) }}" @else src="{{ asset('img/unknown.png') }}" @endif alt="" class="img-circle img-responsive img-profile">

                                        </li>

                                        <li class="list-group-item text-right">
                                            <span class="pull-left">
                                                <strong>Nom</strong>
                                            </span>{{ Auth::user()->name }}</li>
                                        <li class="list-group-item text-right">
                                            <span class="pull-left">
                                                <strong>Nous à Rejoint</strong>
                                            </span>{{ Auth::user()->created_at->format('d-m-Y') }}</li>
                                    </ul>

                                </div>
                                <div class="col-xs-12 col-sm-8 profile-name">
                                    <h2>Mon Profil
                                    </h2>
                                    <hr>
                                    <form action="{{ route('handleUpdateUserProfile') }}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                    <dl class="dl-horizontal-profile">
                                        <dt>Nom</dt>
                                        <dd><input class="form-control" value="{{ Auth::user()->name }}" required name="name" type="text"></dd>

                                        <dt>Description</dt>
                                        <dd><input class="form-control" value="{{ Auth::user()->description }}" name="description" type="text"></dd>

                                        <dt>Email</dt>
                                        <dd><input class="form-control" value="{{ Auth::user()->email }}" name="email" type="email"></dd>

                                        <dt>Nouveau Mot de Passe</dt>
                                        <dd><input class="form-control" value="" name="password" type="password"></dd>

                                        <dt>Téléphone</dt>
                                        <dd><input class="form-control" value="{{ Auth::user()->phone }}" name="phone" type="number"></dd>

                                        <dt>Photo</dt>
                                        <dd><input class="form-control" name="photo" type="file"></dd>

                                        <div class="pull-right">
                                            <button type="submit" class="btn btn-primary">Enregistrer</button>
                                        </div>
                                    </dl>
                                    </form>


                                    <hr>

                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <!-- END OF PROFILE -->


                <!-- /END OF CONTENT -->



                <!-- FOOTER -->
                <div class="footer-space"></div>
                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate"></p>
                        <p id="clock"></p>
                    </div>
                    <div class="copyright">Dévéloppé avec
                        <span class="entypo-heart"></span> par <a href="https://www.bluepenlabs.com" target="_blank">BluePen Labs</a></div>
                    <div class="devider-footer"></div>

                </div>
                <!-- / END OF FOOTER -->


            </div>

        </div>
    </div>

@endsection


@extends('frontOffice.layout')

@section('head')
    @include('frontOffice.inc.head',
    ['title' => config('app.name') . ' - Liste des Utilisateurs',
    'description' => 'Liste des Utilisateurs - ' . config('app.name')
    ])
@endsection

@section('header')
    @include('frontOffice.inc.header')
@endsection

@section('sidebar-left')
    @include('frontOffice.inc.sidebar.left', [
        'current' => 'users'
    ])
@endsection

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatable/datatable.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/datatable/datatable.js') }}"></script>

    <script type="text/javascript">

        /* Datatables responsive */

        $(document).ready(function () {
            $('#datatable-responsive').DataTable({
                responsive: true,
                language: {
                    url: "{{ asset('plugins/datatable/lang/fr.json') }}"
                }
            });
            $('.dataTables_filter input').attr("placeholder", "Rechercher...");
        });

    </script>

    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i>
                            <span>Annonces
                            </span>
                        </h2>

                    </div>

                    <div class="col-lg-9">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                            <div class="alert">
                                <span class="tittle-alert entypo-info-circled"></span>
                                {!! $announcement !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="{{ route('showHome') }}">Accueil</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="{{ route('showManagerUsersList') }}">Utilisateurs</a>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->


            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
                <div class="row">


                    <div class="col-sm-12">

                        <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <h6>
                                    Liste des Utilisateurs</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#FootableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#Footable">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="Footable">

                                <table id="datatable-responsive"
                                       class="display table table-striped table-bordered" cellspacing="0"
                                       width="100%">
                                    <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th>Email</th>
                                        <th>Téléphone</th>
                                        <th>Rôle</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tfoot>
                                    <tr>
                                        <th>Nom</th>
                                        <th>Email</th>
                                        <th>Téléphone</th>
                                        <th>Rôles</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>

                                    <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->phone }}</td>
                                            <td>@foreach($user->roles as $role) {{ $role->title }} @endforeach</td>
                                            <td style="text-align: center">
                                                <span class="badge badge-pill " title="Modifier" data-toggle="modal" data-target="#edit-{{ $user->id }}"><i class="fa fa-edit"></i></span>
                                                @if($user->status == 1)
                                                <a href="{{ route('handleManagerBanUser', ['id' => $user->id]) }}"><span class="badge badge-pill warning-color"><i class="fa fa-trash-o"></i></span></a>
                                                    @else
                                                    <a href="{{ route('handleManagerActivateUser', ['id' => $user->id]) }}"><span class="badge badge-pill info-color"><i class="fa fa-user"></i></span></a>
                                                @endif
                                            </td>
                                        </tr>
                                        <div class="modal fade" id="edit-{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <form method="post" enctype="multipart/form-data" action="{{ route('handleManagerEditUser', ['id' => $user->id]) }}">
                                                        {{ csrf_field() }}
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Modifier {{ $user->name }}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <label class="control-label">Nom</label>
                                                                    <input name="name" required class="form-control" value="{{ $user->name }}">
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <label class="control-label">Email</label>
                                                                    <input name="email" required class="form-control" value="{{ $user->email }}">
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <label class="control-label">Nouveau Mot de Passe</label>
                                                                    <input name="password" class="form-control" type="password">
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <label class="control-label">Description</label>
                                                                    <input name="description" class="form-control" value="{{ $user->description }}">
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <label class="control-label">Téléphone</label>
                                                                    <input name="phone" class="form-control" placeholder="" type="number" value="{{ $user->phone }}">
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <label class="control-label">Photo @if($user->photo) <sup><a target="_blank" href="{{ asset($user->photo) }}">Voir</a> </sup>@endif</label>
                                                                    <input name="photo" class="form-control" type="file">
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <label class="control-label">Rôles</label><br>
                                                                    @foreach($roles as $role)
                                                                        <input type="checkbox" name="roles[]" autocomplete="off" @if(checkUnspecificRole($user, $role->id)) checked="checked" @endif value="{{ $role->id }}"> {{ $role->title }}
                                                                    @endforeach
                                                                </div>
                                                                    <div class="col-lg-12">
                                                                        <label class="control-label">Hôpital:</label>
                                                                        <div class="ui-select">
                                                                            <select class="form-control" name="hospital">
                                                                                <option>Aucun</option>
                                                                                @foreach($unaffected as $hospital)
                                                                                    <option @if(checkMedicalAgentRole($user) and $user->hospital_id == $hospital->id) selected @endif value="{{ $hospital->id }}">{{ $hospital->title }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                            </div>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                                                            <button type="submit" class="btn btn-primary">Envoyer</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>


                    <div class="col-sm-12">

                        <div class="nest" id="Footable2Close">
                            <div class="title-alt">
                                <h6>
                                    Nouvel Utilisateur</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#Footable2Close">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#Footable2">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="Footable2">

                                <form class="form-horizontal" enctype="multipart/form-data" role="form" action="{{ route('handleManagerAddUser') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Nom :</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" required value="" name="name" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Téléphone :</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="" name="phone" type="number">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Mot de Passe :</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" required value="" name="password" type="password">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Email :</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" required value="" name="email" type="email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Description :</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="" name="description" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Photo :</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" value="" name="photo" type="file">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Rôles :</label>
                                        <div class="col-lg-8">
                                        @foreach($roles as $role)
                                            <input type="checkbox" name="roles[]" autocomplete="off" @if(checkUnspecificRole($user, $role->id)) checked="checked" @endif value="{{ $role->id }}"> {{ $role->title }}
                                        @endforeach
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Hôpital:</label>
                                        <div class="col-lg-8">
                                            <div class="ui-select">
                                                <select class="form-control" name="hospital">
                                                    <option>Aucun</option>
                                                    @foreach($unaffected as $hospital)
                                                        <option value="{{ $hospital->id }}">{{ $hospital->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div class="col-md-8">
                                            <input class="btn btn-primary" value="Ajouter" type="submit">
                                            <span></span>
                                            <input class="btn btn-default" value="Annuler" type="reset">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>


                    </div>

                </div>
            </div>            <!--  / DEVICE MANAGER -->


            <div class="content-wrap">
                <!-- FOOTER -->
                <div class="footer-space"></div>
                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate"></p>
                        <p id="clock"></p>
                    </div>
                    <div class="copyright">Dévéloppé avec
                        <span class="entypo-heart"></span> par <a href="https://www.bluepenlabs.com" target="_blank">BluePen Labs</a>
                    </div>
                    <div class="devider-footer"></div>


                </div>
                <!-- / END OF FOOTER -->


            </div>
        </div>
    </div>

@endsection


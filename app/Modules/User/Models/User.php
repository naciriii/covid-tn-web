<?php

namespace App\Modules\User\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'description',
        'phone',
        'status',
        'photo',
        'provider',
        'provider_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany(
            'App\Modules\User\Models\Role',
            'user_roles',
            'user_id',
            'role_id'
        )->withTimestamps();
    }

    public function hospital()
    {
        return $this->hasOne('App\Modules\Hospital\Models\Hospital', 'user_id', 'id');
    }

    public function volunteer()
    {
        return $this->hasOne('App\Modules\Volunteer\Models\Volunteer', 'user_id', 'id');
    }
}

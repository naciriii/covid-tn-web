<?php

if (!function_exists('checkAdministratorRole')) {
    /**
     * @param \App\Modules\User\Models\User $user
     * @return bool
     */
    function checkAdministratorRole($user)
    {
        foreach ($user->roles as $role) {
            if ($role->id == 1) {
                return true;
            }
        }
        return false;
    }
}

if (!function_exists('checkVolunteerRole')) {
    /**
     * @param \App\Modules\User\Models\User $user
     * @return bool
     */
    function checkVolunteerRole($user)
    {
        foreach ($user->roles as $role) {
            if ($role->id == 2) {
                return true;
            }
        }
        return false;
    }
}

if (!function_exists('checkMedicalAgentRole')) {
    /**
     * @param \App\Modules\User\Models\User $user
     * @return bool
     */
    function checkMedicalAgentRole($user)
    {
        foreach ($user->roles as $role) {
            if ($role->id == 3) {
                return true;
            }
        }
        return false;
    }
}

if (!function_exists('checkUnspecificRole')) {
    /**
     * @param \App\Modules\User\Models\User $user
     * @param $unspecific
     * @return bool
     */
    function checkUnspecificRole($user, $unspecific)
    {
        foreach ($user->roles as $role) {
            if ($role->id == $unspecific) {
                return true;
            }
        }
        return false;
    }
}

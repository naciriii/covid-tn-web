<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use \Kamaln7\Toastr\Facades\Toastr;

class CheckMedicalAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()) {
            return abort(404);
        }
        if (!checkMedicalAgentRole(Auth::user())) {
            Toastr::error('Vous n\'avez pas la permission d\'accéder a cette page');
            return redirect(route('showHome'));
        }

        return $next($request);
    }
}

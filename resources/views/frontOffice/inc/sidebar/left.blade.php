<div id="skin-select">
    <div id="logo">
        <h1>{{ config('app.name') }}<span>v{{ config('app.version') }}</span></h1>
    </div>

    <a id="toggle">
        <span class="entypo-menu"></span>
    </a>

    <div class="skin-part" style="top: -17px;">
        <div id="tree-wrap">
            <div class="side-bar">
                @auth
                @if(checkAdministratorRole(Auth::user()))
                <ul class="topnav menu-left-nest">
                    <li>
                        <a href="#" style="border-left:0px solid!important;" class="title-menu-left">

                            <span class="widget-menu">Administration</span>
                            <i data-toggle="tooltip" class="entypo-cog pull-right config-wrap"></i>

                        </a>
                    </li>
                    <li>
                        <a class="tooltip-tip ajax-load" href="{{ route('showManagerGeneralConfiguration') }}" title="Media">
                            <i class="fa fa-gear"></i>
                            <span>Général</span>

                        </a>
                    </li>
                    <li>
                        <a class="tooltip-tip ajax-load" href="{{ route('showManagerHospitalsList') }}" title="Hôpitaux">
                            <i class="fa fa-hospital-o"></i>
                            <span>Hôpitaux</span>

                        </a>
                    </li>
                    <li>
                        <a class="tooltip-tip ajax-load" href="{{ route('showManagerRequestTypesList') }}" title="Besoins">
                            <i class="fa fa-heart-o"></i>
                            <span>Besoins</span>

                        </a>
                    </li>

                    <li>
                        <a class="tooltip-tip ajax-load" href="{{ route('showManagerUsersList') }}" title="Media">
                            <i class="fa fa-users"></i>
                            <span>Utilisateurs</span>
                        </a>
                    </li>
                </ul>
                @endif

                @if(checkMedicalAgentRole(Auth::user()))
                <ul class="topnav menu-left-nest">
                    <li>
                        <a href="#" style="border-left:0px solid!important;" class="title-menu-left">

                            <span class="design-kit">Médical</span>
                            <i data-toggle="tooltip" class="entypo-cog pull-right config-wrap"></i>

                        </a>
                    </li>

                    <li>
                        <a class="tooltip-tip ajax-load" href="{{ route('showMedicalAgentHospital', ['id' => Auth::user()->hospital->id]) }}" title="Hopital">
                            <i class="fa fa-heart-o"></i>
                            <span>Mon Hôpital</span>

                        </a>
                    </li>
                    <li>
                        <a class="tooltip-tip ajax-load" href="{{ route('showMedicalAgentHospitalRequests', ['id' => Auth::user()->hospital->id]) }}" title="Besoins">
                            <i class="fa fa-hand-o-right"></i>
                            <span>Besoins</span>
                            <div class="noft-blue">{{ \Illuminate\Support\Facades\Auth::user()->hospital->requests->count() }}</div>
                        </a>
                    </li>
                </ul>
                @endif

                    @if(checkVolunteerRole(Auth::user()))
                        <ul class="topnav menu-left-nest">
                            <li>
                                <a href="#" style="border-left:0px solid!important;" class="title-menu-left">

                                    <span class="design-kit">Bénévolat</span>
                                    <i data-toggle="tooltip" class="entypo-cog pull-right config-wrap"></i>

                                </a>
                            </li>

                            <li>
                                <a class="tooltip-tip ajax-load" href="{{ route('showVolunteerUpdateProfile') }}" title="J'Aide">
                                    <i class="fa fa-gift"></i>
                                    <span>J'AIDE</span>
                                </a>
                            </li>
                        </ul>
                    @endif

                @endauth
                    <ul class="topnav menu-left-nest">
                        <li>
                            <a href="#" style="border-left:0px solid!important;" class="title-menu-left">

                                <span class="widget-menu">Menu Principal</span>
                                <i data-toggle="tooltip" class="entypo-cog pull-right config-wrap"></i>

                            </a>
                        </li>
                        <li>
                            <a class="tooltip-tip ajax-load" href="{{ route('showHome') }}" title="Media">
                                <i class="fa fa-home"></i>
                                <span>Accueil</span>
                            </a>
                        </li>
                        @auth()

                        <li>
                            <a class="tooltip-tip ajax-load" href="{{ route('showUserProfile') }}" title="Profil">
                                <i class="fa fa-user"></i>
                                <span>Mon Profil</span>

                            </a>
                        </li>
                            <li>
                                <a class="tooltip-tip ajax-load" href="{{ route('handleLogout') }}" title="Déconnexion">
                                    <i class="fa fa-lock"></i>
                                    <span>Déconnexion</span>

                                </a>
                            </li>
                        @else
                            <li>
                                <a class="tooltip-tip ajax-load signin-popup" title="Inscription">
                                    <i class="fa fa-gear"></i>
                                    <span>Inscription</span>

                                </a>
                            </li>
                            @endif
                        <li>
                            <a class="tooltip-tip ajax-load" href="{{ route('showContact') }}" title="Contact">
                                <i class="fa fa-mail-forward"></i>
                                <span>Contact</span>

                            </a>
                        </li>
                    </ul>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('plugins/progress-bar/src/jquery.velocity.min.js') }}"></script>
<script src="{{ asset('plugins/progress-bar/number-pb.js') }}"></script>
<script src="{{ asset('plugins/progress-bar/progress-app.js') }}"></script>



<!-- MAIN EFFECT -->
<script type="text/javascript" src="{{ asset('js/frontOffice/preloader.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/bootstrap/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/frontOffice/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/frontOffice/load.js') }}"></script>

<!-- GAGE -->


<script type="text/javascript" src="{{ asset('plugins/countdown/countdown.js') }}"></script>

<script src="{{ asset('plugins/jhere/jhere-custom.js') }}"></script>

<script>
    //Sliding Effect Control
    head.js("{{ asset('/') }}/plugins/skin-select/jquery.cookie.js");
    head.js("{{ asset('/') }}/plugins/skin-select/skin-select.js");

    //Showing Date
    head.js("{{ asset('/') }}/plugins/clock/date.js");

    //NEWS STICKER
    head.js("{{ asset('/') }}/plugins/newsticker/jquery.newsTicker.js", function() {

        var nt_title = $('#nt-title').newsTicker({
            row_height: 18,
            max_rows: 1,
            duration: 5000,
            pauseOnHover: 0
        });


    });

    //------------------------------------------------------------- 


    ////Acordion and Sliding menu

    head.js("{{ asset('/') }}/plugins/custom/scriptbreaker-multiple-accordion-1.js", function() {

        $(".topnav").accordionze({
            accordionze: true,
            speed: 500,
            closedSign: '<img src="img/plus.png">',
            openedSign: '<img src="img/minus.png">'
        });

    });

    ////Right Sliding menu

    head.js("{{ asset('/') }}/plugins/slidebars/slidebars.min.js", "https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js", function() {

        $(document).ready(function() {
            var mySlidebars = new $.slidebars();

            $('.toggle-left').on('click', function() {
                mySlidebars.toggle('right');
            });
        });
    });

    //-------------------------------------------------------------

    //SEARCH MENU
    head.js("{{ asset('/') }}/plugins/search/jquery.quicksearch.js", function() {

        $('input.id_search').quicksearch('#menu-showhide li, .menu-left-nest li');



    });
    //-------------------------------------------------------------



    //EASY PIE CHART
    head.js("{{ asset('/') }}/plugins/gage/jquery.easypiechart.min.js", function() {

        $(function() {
            
            $('.chart').easyPieChart({
                easing: 'easeOutBounce',
                trackColor: '#ffffff',
                scaleColor: '#ffffff',
                barColor: '#FF0064',
                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });
            var chart = window.chart = $('.chart').data('easyPieChart');
            $('.js_update').on('click', function() {
                chart.update(Math.random() * 100);
            });

            $('.speed-car').easyPieChart({
                easing: 'easeOutBounce',
                trackColor: 'rgba(0,0,0,0.3)',
                scaleColor: 'transparent',
                barColor: '#0085DF',

                lineWidth: 8,
                onStep: function(from, to, percent) {
                    $(this.el).find('.percent2').text(Math.round(percent));
                }
            });
            var chart = window.chart = $('.chart2').data('easyPieChart');
            $('.js_update').on('click', function() {
                chart.update(Math.random() * 100);
            });
            $('.overall').easyPieChart({
                easing: 'easeOutBounce',
                trackColor: 'rgba(0,0,0,0.3)',
                scaleColor: '#323A45',
                lineWidth: 35,
                lineCap: 'butt',
                barColor: '#FFB900',
                onStep: function(from, to, percent) {
                    $(this.el).find('.percent3').text(Math.round(percent));
                }
            });
        });

    });
    //-------------------------------------------------------------

    //TOOL TIP

    head.js("{{ asset('/') }}/plugins/tip/jquery.tooltipster.js", function() {

        $('.tooltip-tip-x').tooltipster({
            position: 'right'

        });

        $('.tooltip-tip').tooltipster({
            position: 'right',
            animation: 'slide',
            theme: '.tooltipster-shadow',
            delay: 1,
            offsetX: '-12px',
            onlyOne: true

        });
        $('.tooltip-tip2').tooltipster({
            position: 'right',
            animation: 'slide',
            offsetX: '-12px',
            theme: '.tooltipster-shadow',
            onlyOne: true

        });
        $('.tooltip-top').tooltipster({
            position: 'top'
        });
        $('.tooltip-right').tooltipster({
            position: 'right'
        });
        $('.tooltip-left').tooltipster({
            position: 'left'
        });
        $('.tooltip-bottom').tooltipster({
            position: 'bottom'
        });
        $('.tooltip-reload').tooltipster({
            position: 'right',
            theme: '.tooltipster-white',
            animation: 'fade'
        });
        $('.tooltip-fullscreen').tooltipster({
            position: 'left',
            theme: '.tooltipster-white',
            animation: 'fade'
        });
        //For icon tooltip



    });
    //------------------------------------------------------------- 

    //NICE SCROLL

    head.js("{{ asset('/') }}/plugins/nano/jquery.nanoscroller.js", function() {

        $(".nano").nanoScroller({
            //stop: true 
            scroll: 'top',
            scrollTop: 0,
            sliderMinHeight: 40,
            preventPageScrolling: true
            //alwaysVisible: false

        });

    });
    //------------------------------------------------------------- 






    //------------------------------------------------------------- 
    //PAGE LOADER
    head.js("{{ asset('/') }}/plugins/pace/pace.js", function() {

        paceOptions = {
            ajax: false, // disabled
            document: false, // disabled
            eventLag: false, // disabled
            elements: {
                selectors: ['.my-page']
            }
        };

    });

    //------------------------------------------------------------- 

    //SPARKLINE CHART
    head.js("{{ asset('/') }}/plugins/chart/jquery.sparkline.js", function() {

        $(function() {
            $('.inlinebar').sparkline('html', {
                type: 'bar',
                barWidth: '8px',
                height: '30px',
                barSpacing: '2px',
                barColor: '#A8BDCF'
            });
            $('.linebar').sparkline('html', {
                type: 'bar',
                barWidth: '5px',
                height: '30px',
                barSpacing: '2px',
                barColor: '#44BBC1'
            });
            $('.linebar2').sparkline('html', {
                type: 'bar',
                barWidth: '5px',
                height: '30px',
                barSpacing: '2px',
                barColor: '#AB6DB0'
            });
            $('.linebar3').sparkline('html', {
                type: 'bar',
                barWidth: '5px',
                height: '30px',
                barSpacing: '2px',
                barColor: '#19A1F9'
            });
        });

        $(function() {
            var sparklineLogin = function() {
                $('#sparkline').sparkline(
                    [5, 6, 7, 9, 9, 5, 3, 2, 2, 4, 6, 7], {
                        type: 'line',
                        width: '100%',
                        height: '25',
                        lineColor: '#ffffff',
                        fillColor: '#0DB8DF',
                        lineWidth: 1,
                        spotColor: '#ffffff',
                        minSpotColor: '#ffffff',
                        maxSpotColor: '#ffffff',
                        highlightSpotColor: '#ffffff',
                        highlightLineColor: '#ffffff'
                    }
                );
            }
            var sparkResize;
            $(window).resize(function(e) {
                clearTimeout(sparkResize);
                sparkResize = setTimeout(sparklineLogin, 500);
            });
            sparklineLogin();
        });


    });

    //------------------------------------------------------------- 

    //DIGITAL CLOCK
    head.js("{{ asset('/') }}/plugins/clock/jquery.clock.js", function() {

        //clock
        $('#digital-clock').clock({
            offset: '+5',
            type: 'digital'
        });


    });

    $(document).on('ready',function(){
        "use strict";

        $('.signin-popup').on('click', function(){
        $('.signin-popup-box').fadeIn('fast');
        $('html').addClass('no-scroll');
        });
        $('.close-popup').on('click', function(){
            $('.signin-popup-box').fadeOut('fast');
            $('html').removeClass('no-scroll');
        });
    });

</script>

<script type="text/javascript">
    var output, started, duration, desired;

    // Constants
    duration = 5000;
    desired = '50';

    // Initial setup
    output = $('#speed');
    started = new Date().getTime();

    // Animate!
    animationTimer = setInterval(function() {
        // If the value is what we want, stop animating
        // or if the duration has been exceeded, stop animating
        if (output.text().trim() === desired || new Date().getTime() - started > duration) {
            console.log('animating');
            // Generate a random string to use for the next animation step
            output.text('' + Math.floor(Math.random() * 60)

            );

        } else {
            console.log('animating');
            // Generate a random string to use for the next animation step
            output.text('' + Math.floor(Math.random() * 120)

            );
        }
    }, 5000);
</script>
<script type="text/javascript">

    $('#getting-started').countdown('2015/01/01', function(event) {
        $(this).html(event.strftime(

            '<span>%M</span>' + '<span class="start-min">:</span>' + '<span class="start-min">%S</span>'));
    });
</script>

<script type="text/javascript">
    var Tawk_API=Tawk_API||{};
    @auth
        Tawk_API.visitor = {
        name  : '{{ Auth::user()->name }}',
        email : '{{ Auth::user()->email }}',
        hash : '{{  hash_hmac('sha256', Auth::user()->email, 'bad7e992243dad13a13114af3e40b85af2c5ed2b') }}'
    };
            @endauth
    var Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5e7f9ef535bcbb0c9aab69b9/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>


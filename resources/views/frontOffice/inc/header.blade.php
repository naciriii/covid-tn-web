<nav role="navigation" class="navbar navbar-static-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle" type="button">
                <span class="entypo-menu"></span>
            </button>




            <div id="logo-mobile" class="visible-xs">
                <h1>Covid-TN<span>v{{ config('app.version') }}</span></h1>
            </div>

        </div>


        <!-- Collect the nav links, forms, and other content for toggling -->
        <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
            <ul style="margin-right:0;" class="nav navbar-nav navbar-right">
                <li>
                    @auth()
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <img alt="" class="admin-pic img-circle" @if(Auth::user()->photo)src="{{ asset(Auth::user()->photo) }}" @else src="{{ asset('img/unknown.png') }}" @endif>Salut {{ Auth::user()->name }}<b class="caret"></b>
                        </a>
                        <ul style="margin-top:14px;" role="menu" class="dropdown-setting dropdown-menu">
                            <li>
                                <a href="{{ route('showUserProfile') }}">
                                    <span class="entypo-user"></span>&#160;&#160;Mon Profil</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="{{ route('handleLogout') }}">
                                    <span class="entypo-lock"></span>&#160;&#160; Déconnexion</a>
                            </li>
                        </ul>
                        @else
                    <a data-toggle="dropdown" class="dropdown-toggle signin-popup" href="#">
                        <img alt="" class="admin-pic img-circle" src="{{ asset('img/unknown.png') }}">Connexion

                    </a>

                        @endauth
                </li>
            </ul>

        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
@if(!\Illuminate\Support\Facades\Auth::user())
<div class="account-popup-area signin-popup-box">
    <div class="account-popup">
        <span class="close-popup"><i class="fa fa-times"></i></span>
        <h3>Espace Membres</h3>

        <form action="{{ route('handleUserRegisterLogin') }}" method="post">
            {{ csrf_field() }}
            <div class="cfield">
                <input type="email" placeholder="fighters@covid.tn" name="email" required/>
                <i class="fa fa-user"></i>
            </div>
            <div class="cfield">
                <input type="password" placeholder="********" autocomplete="off" name="password" required/>
                <i class="fa fa-key"></i>
            </div>
            <div class="row" style="padding: 0px;">
                <div class="col-md-6">
                    <button name="login" class="login" value="1" type="submit">Connexion</button>
                </div>
                <div class="col-md-6">
                    <button name="login" class="signup" value="2"  type="submit">Inscription</button>
                </div>
            </div>
        </form>
        <div class="extra-login">
            <span>Où</span>
            <div class="login-social">
                <a class="fb-login" href="{{ route('handleSocialRedirect', ['provider' => 'facebook']) }}" title=""><i class="fa fa-facebook"></i></a>
            </div>
        </div>
    </div>
</div><!-- LOGIN POPUP -->
@endif

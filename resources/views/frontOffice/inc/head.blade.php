<head>
    <meta charset="utf-8">
    <title>{{ $title }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="BluePen Labs">
    <script type="text/javascript" src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

    <link rel="stylesheet" href="{{ asset('css/frontOffice/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/frontOffice/loader-style.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/bootstrap.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset ('plugins/toastr/toastr.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/progress-bar/number-pb.css') }}">

    <style type="text/css">
        canvas#canvas4 {
            position: relative;
            top: 20px;
        }
    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-33598597-9"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-33598597-9');
    </script>


    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="{{ asset('img/minus.png') }}">
</head>

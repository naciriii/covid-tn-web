<!DOCTYPE html>
<html lang="fr">

@yield('head')
<body>
{!! Toastr::render() !!}

<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<!-- TOP NAVBAR -->
@yield('header')

@yield('sidebar-left')
@yield('content')

@include('frontOffice.inc.scripts')

</body>

</html>

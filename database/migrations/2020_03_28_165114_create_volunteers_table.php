<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVolunteersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('volunteering_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->timestamps();
        });

        Schema::create('volunteers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('age');
            $table->integer('gender');
            $table->string('organisation')->nullable();
            $table->float('lat')->nullable();
            $table->float('lng')->nullable();
            $table->text('days');
            $table->integer('status')->default(1);
            $table->timestamps();
        });

        Schema::create('volunteer_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('volunteer_id');
            $table->foreign('volunteer_id')->references('id')->on('volunteers');
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('volunteering_categories');
            $table->text('details')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('volunteer_categories');
        Schema::dropIfExists('volunteers');
        Schema::dropIfExists('volunteering_categories');
    }
}

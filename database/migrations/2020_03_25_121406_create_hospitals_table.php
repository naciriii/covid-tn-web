<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHospitalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospitals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->integer('type')->default(1); // 1 public / 2 private / 3 militaire / 4 instit
            $table->float('lat');
            $table->float('lng');
            $table->integer('phone')->nullable();
            $table->text('address')->nullable();
            $table->unsignedBigInteger('city_id')->nullable();
            $table->foreign('city_id')->references('id')->on('cities');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->bigInteger('rib')->nullable();
            $table->string('iban')->nullable();
            $table->string('bank')->nullable();
            $table->string('bic_swift')->nullable();
            $table->timestamps();
        });

        Schema::create('requests_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->float('min')->nullable();
            $table->float('max')->nullable();
            $table->timestamps();
        });

        Schema::create('hospital_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('request_type_id');
            $table->foreign('request_type_id')->references('id')->on('requests_types');
            $table->unsignedBigInteger('hospital_id');
            $table->foreign('hospital_id')->references('id')->on('hospitals');
            $table->text('details')->nullable();
            $table->integer('auto_renew')->default(0);
            $table->integer('renewal_days')->default(0);
            $table->integer('quantity')->default(1);
            $table->integer('severity')->default(1);
            $table->integer('status')->default(1);
            $table->timestamps();
        });

        Schema::create('hospital_requests_entries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('request_id');
            $table->foreign('request_id')->references('id')->on('hospital_requests');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('quantity')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hospital_requests_entries');
        Schema::dropIfExists('hospital_requests');
        Schema::dropIfExists('requests_types');
        Schema::dropIfExists('hospitals');
    }
}

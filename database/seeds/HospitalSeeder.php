<?php

use App\Modules\Hospital\Models\Hospital;
use Illuminate\Database\Seeder;

class HospitalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Hospital::create([
            'title' => 'Hôpital Habib Thameur',
            'phone' => 71397000,
            'type' => 1,
            'address' => 'Rue Ali Ben Ayed',
            'city_id' => 23,
            'lat' => 36.7865739,
            'lng' => 10.1706613,
        ]);

        Hospital::create([
            'title' => 'Hôpital Charles-Nicolle',
            'type' => 1,
            'address' => 'boulevard du 9-Avril 1938',
            'city_id' => 23,
            'lat' => 36.8023276,
            'lng' => 10.1609419,
        ]);

        Hospital::create([
            'title' => 'Hôpital Aziza Othmana',
            'type' => 1,
            'address' => 'La Kasbah',
            'phone' => 71560763,
            'city_id' => 23,
            'lat' => 36.7969453,
            'lng' => 10.1664633,
        ]);

        Hospital::create([
            'title' => 'Hôpital La Rabta',
            'type' => 1,
            'address' => 'Rabta',
            'phone' => 71562083,
            'city_id' => 23,
            'lat' => 36.8018198,
            'lng' => 10.1523812,
        ]);

        Hospital::create([
            'title' => 'Hôpital Militaire Primaire d\'Instruction de Tunis',
            'type' => 3,
            'address' => 'Tunis',
            'phone' => 71391133,
            'city_id' => 23,
            'lat' => 36.7857809,
            'lng' => 10.1761911,
        ]);

        Hospital::create([
            'title' => 'Centre Hospitalier International Carthagène',
            'type' => 2,
            'address' => 'Tunis',
            'phone' => 31336336,
            'city_id' => 23,
            'lat' => 36.8453714,
            'lng' => 10.1980071,
        ]);

        Hospital::create([
            'title' => 'Hôpital Mongi-Slim de La Marsa',
            'type' => 1,
            'address' => 'La Marsa',
            'phone' => 70109100,
            'city_id' => 23,
            'lat' => 36.8662702,
            'lng' => 10.287839,
        ]);

        Hospital::create([
            'title' => 'Centre de traumatologie et des grands brûlés',
            'type' => 1,
            'address' => 'Rue du 1er mai, Ben Arous 2013',
            'phone' => 79100500,
            'city_id' => 3,
            'lat' => 36.747261,
            'lng' => 10.2113143,
        ]);

        Hospital::create([
            'title' => 'Hôpital Abderrahmen Mami de Pneumo-phtisiologie',
            'type' => 1,
            'address' => 'Ariana',
            'phone' => 71713645,
            'city_id' => 1,
            'lat' => 36.8698022,
            'lng' => 10.1760542,
        ]);

        Hospital::create([
            'title' => 'Hôpital des forces de sécurité intérieure de La Marsa',
            'type' => 3,
            'address' => '29 Rue Tahar Ben Achour, Marsa',
            'phone' => 52812912,
            'city_id' => 23,
            'lat' => 36.879037,
            'lng' => 10.3228032,
        ]);

        Hospital::create([
            'title' => 'Hôpital régional de Kheireddine',
            'type' => 1,
            'address' => 'La Goulette',
            'phone' => 71731860,
            'city_id' => 23,
            'lat' => 36.8292436,
            'lng' => 10.3132438,
        ]);

        Hospital::create([
            'title' => 'Hôpital régional de Ben Arous',
            'type' => 1,
            'address' => 'Ben Arous',
            'phone' => 71315600,
            'city_id' => 3,
            'lat' => 36.7323581,
            'lng' => 10.245164,
        ]);

        Hospital::create([
            'title' => 'Hôpital régional Mohamed Taher El Maamouri',
            'type' => 1,
            'address' => 'Mrezga 8000',
            'phone' => 72285022,
            'city_id' => 16,
            'lat' => 36.4377244,
            'lng' => 10.6720279,
        ]);

        Hospital::create([
            'title' => 'CHU Farhat Hached',
            'type' => 4,
            'address' => 'Ibn Jazzar, Sousse',
            'phone' => 73102500,
            'city_id' => 20,
            'lat' => 35.8296103,
            'lng' => 10.6255607,
        ]);

        Hospital::create([
            'title' => 'Hôpital Régional de Kairouan',
            'type' => 1,
            'address' => 'Kairouan',
            'city_id' => 8,
            'lat' => 35.66857,
            'lng' => 10.1046853,
        ]);

        Hospital::create([
            'title' => 'Hôpital Ennadhour',
            'type' => 1,
            'address' => 'C132, Nadour',
            'phone' => 72123456,
            'city_id' => 24,
            'lat' => 36.1235501,
            'lng' => 10.0868969,
        ]);

        Hospital::create([
            'title' => 'Hôpital Fattouma-Bourguiba de Monastir',
            'type' => 1,
            'address' => 'نهج غرة جوان 1955، Monastir',
            'phone' => 73908700,
            'city_id' => 15,
            'lat' => 35.7697724,
            'lng' => 10.8314178,
        ]);

        Hospital::create([
            'title' => 'Hôpital Fattouma-Bourguiba de Monastir',
            'type' => 1,
            'address' => 'نهج غرة جوان 1955، Monastir',
            'phone' => 73908700,
            'city_id' => 15,
            'lat' => 35.7697724,
            'lng' => 10.8314178,
        ]);

        Hospital::create([
            'title' => 'Hôpital CHU Fattouma Bourguiba',
            'type' => 4,
            'address' => 'نهج غرة جوان 1955، Monastir',
            'phone' => 73461141,
            'city_id' => 15,
            'lat' => 35.7703318,
            'lng' => 10.8315767,
        ]);

        Hospital::create([
            'title' => 'Hôpital Ibn Eljazzar',
            'type' => 1,
            'address' => 'Rue Ibn El Jazzar، Kairouan 3100',
            'phone' => 77226300,
            'city_id' => 8,
            'lat' => 35.6840393,
            'lng' => 10.0935841,
        ]);

        Hospital::create([
            'title' => 'Hôpital Universitaire Sahloul (CHU Sahloul)',
            'type' => 4,
            'address' => 'Sousse',
            'phone' => 73369411,
            'city_id' => 8,
            'lat' => 35.8371356,
            'lng' => 10.588292,
        ]);

        Hospital::create([
            'title' => 'Hôpital Universitaire Tahar Sfar',
            'type' => 4,
            'address' => 'Hôpital Universitaire Tahar Sfar, Mahdia 5100',
            'phone' => 73109000,
            'city_id' => 12,
            'lat' => 35.5100753,
            'lng' => 11.0304542,
        ]);

        Hospital::create([
            'title' => 'Hôpital Bouficha',
            'type' => 1,
            'address' => 'C35, Bouficha',
            'city_id' => 16,
            'lat' => 36.3012414,
            'lng' => 10.4515553,
        ]);

        Hospital::create([
            'title' => 'Hôpital Chorbane',
            'type' => 1,
            'address' => 'C96, Surban',
            'phone' => 73600070,
            'city_id' => 12,
            'lat' => 35.2884259,
            'lng' => 10.3872889,
        ]);

        Hospital::create([
            'title' => 'Hôpital Chorbane',
            'type' => 1,
            'address' => 'C96, Surban',
            'phone' => 73600070,
            'city_id' => 12,
            'lat' => 35.2884259,
            'lng' => 10.3872889,
        ]);

        Hospital::create([
            'title' => 'Hôpital Régional de M\'saken',
            'type' => 1,
            'address' => 'M\'saken',
            'phone' => 73259106,
            'city_id' => 20,
            'lat' => 35.7373643,
            'lng' => 10.5703764,
        ]);

        Hospital::create([
            'title' => 'Hôpital Local Hammamet',
            'type' => 1,
            'address' => 'Hammamet',
            'city_id' => 16,
            'lat' => 36.3997293,
            'lng' => 10.613148,
        ]);

        Hospital::create([
            'title' => 'Clinique les Violettes',
            'type' => 2,
            'address' => 'Route d\'Hammamet Nabeul, BP 14, 8000 Nabeul، Avenue Dali Jazi, Nabeul‎ 8000',
            'phone' => 72224000,
            'city_id' => 16,
            'lat' => 36.4300293,
            'lng' => 10.6813593,
        ]);

        Hospital::create([
            'title' => 'Clinique Les Oliviers',
            'type' => 2,
            'address' => 'Boulevard du 14 Janvier, Sousse 4051',
            'phone' => 73242711,
            'city_id' => 20,
            'lat' => 35.8549983,
            'lng' => 10.6128919,
        ]);

        Hospital::create([
            'title' => 'Centre International Carthage Médical',
            'type' => 2,
            'address' => 'Centre International Carthage Médical, 5000',
            'phone' => 73524000,
            'city_id' => 23,
            'lat' => 35.760933,
            'lng' => 10.7267303,
        ]);

        Hospital::create([
            'title' => 'Hôpital Hédi Chaker',
            'type' => 1,
            'address' => 'Route El Ain, Sfax 3089',
            'phone' => 74244422,
            'city_id' => 17,
            'lat' => 34.7407245,
            'lng' => 10.7481151,
        ]);

        Hospital::create([
            'title' => 'Hôpital régional Houcine Bouzaiene',
            'type' => 1,
            'address' => 'Gafsa',
            'phone' => 76100200,
            'city_id' => 6,
            'lat' => 34.420007,
            'lng' => 8.7939595,
        ]);

        Hospital::create([
            'title' => 'Hôpital Regional de Zarzis',
            'type' => 1,
            'address' => 'Dhouiher',
            'phone' => 75738026,
            'city_id' => 22,
            'lat' => 33.5414606,
            'lng' => 11.086284,
        ]);

        Hospital::create([
            'title' => 'Hôpital Régional de Tataouine',
            'type' => 1,
            'address' => 'Cité Tahrir',
            'city_id' => 21,
            'lat' => 32.9845911,
            'lng' => 10.4587634,
        ]);

        Hospital::create([
            'title' => 'Hôpital Midoun',
            'type' => 1,
            'phone' => 75734014,
            'address' => 'Djerba Midun',
            'city_id' => 14,
            'lat' => 33.8131631,
            'lng' => 10.9887719,
        ]);

        Hospital::create([
            'title' => 'Hôpital Sadok Mokaddem',
            'type' => 1,
            'phone' => 75650018,
            'address' => 'Houmt Souk',
            'city_id' => 14,
            'lat' => 33.8670838,
            'lng' => 10.8635473,
        ]);

        Hospital::create([
            'title' => 'Hôpital Régional Habib Bourguiba de Médenine',
            'type' => 1,
            'phone' => 75640044,
            'address' => 'Médenine',
            'city_id' => 14,
            'lat' => 33.3319387,
            'lng' => 10.4855965,
        ]);

        Hospital::create([
            'title' => 'Hôpital local de Mdhilla',
            'type' => 1,
            'phone' => 76266250,
            'address' => 'Mdhila',
            'city_id' => 6,
            'lat' => 34.2868884,
            'lng' => 8.7486828,
        ]);
    }
}

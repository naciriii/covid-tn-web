<?php

use App\Modules\Volunteer\Models\Category;
use Illuminate\Database\Seeder;

class VolunteerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Category::create([
            'title' => 'Financer',
        ]);

        Category::create([
            'title' => 'Transporter',
        ]);

        Category::create([
            'title' => 'Soigner',
        ]);

        Category::create([
            'title' => 'Donner du Sang',
        ]);

        Category::create([
            'title' => 'Cuisiner',
        ]);

        Category::create([
            'title' => 'Faire les courses',
        ]);

        Category::create([
            'title' => 'Imprimer',
        ]);

        Category::create([
            'title' => 'Confectionner',
        ]);

        Category::create([
            'title' => 'Programmer',
        ]);

        Category::create([
            'title' => 'Autre',
        ]);
    }
}
